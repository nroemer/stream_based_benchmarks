# Stream_Based_Benchmarks

Gitlab repository for benchmarking and developing stream based communication on the AI Engine for the VCK 190 Xilinx Versal developer board
This repository was developed for the Bachelor Thesis 'Designing of a communication library for Versal devices using
Stream-Based API'

It is built on top of https://spclgitlab.ethz.ch/tizianode/exploring_versal by Tiziano De Matteis and Max Wierse
The Makefile used for all programs in this repository is from this repository.

# Structure
All code referenced in the thesis can be found in [```code```](code/).

- [```code/bandwidth_benchmark/circuit```](code/bandwidth_benchmark/circuit): Implementation of a Bandwidth Benchmark using circuit switching.
- [```code/bandwidth_benchmark/packet```](code/bandwidth_benchmark/packet): Implementation of a Bandwidth Benchmark using packet switching.
- [```code/broadcast_benchmark/circuit```](code/broadcast_benchmark/circuit): Implementation of a Broadcast Benchmark using circuit switching.
- [```code/broadcast_benchmark/packet```](code/broadcast_benchmark/packet): Implementation of a Broadcast Benchmark using packet switching.
- [```code/latency_benchmark/circuit```](code/latency_benchmark/circuit): Implementation of a Latency Benchmark using circuit switching.
- [```code/latency_benchmark/packet```](code/latency_benchmark/packet): Implementation of a ping pong using packet switching (not working).
- [```code/reduce```](code/reduce): Implementations of all the communication models for a Reduce operation mentioned in the thesis.
- [```code/gather```](code/gather): Implementation of a Gather as a reference point for future work.
- [```code/scatter```](code/scatter): Implementation of a Scatter as baseline for future work.
- [```code/allreduce```](code/allreduce): Idea of how an Allreduce could be implemented.
