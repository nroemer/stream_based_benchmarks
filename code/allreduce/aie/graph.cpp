#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <adf.h>
using namespace adf;
simpleGraph mygraph;

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
#endif


int main(int argc, char *argv[])
{
  adf::return_code ret;

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif


  uint64 *time = (uint64 *)GMIO::malloc(1 * sizeof(uint64));

  mygraph.gmio_time.aie2gm_nb(time, 1 * sizeof(uint64));

  mygraph.init();

  ret = mygraph.run(1);
  if (ret != adf::ok){
    printf("Host: Run failed\n");
    return ret;
  }

  mygraph.gmio_time.wait();

  mygraph.wait();

  ret = mygraph.end();
  if (ret != adf::ok) {
    printf("Host: End failed\n");
    return ret;
  }

  printf("It took %llu cycles\n", time[0]);
  
  GMIO::free(time);

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}

