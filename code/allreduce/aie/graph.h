#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nchilds 62
#define nleafs 64


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel childk[nchilds];
  kernel leafk[nleafs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(16, 4);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[1], gmio_time.in[0]);

    for (int i = 0; i < nchilds; i++) {
      childk[i] = kernel::create(child);
      source(childk[i]) = "kernels.cpp";
      runtime<ratio>(childk[i]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 8) {
        location<kernel>(childk[i]) = tile(2, i);
        connect<stream> net1(childk[i].out[0], childk[i+16].in[0]);
      }
      else if (i < 16) {
        location<kernel>(childk[i]) = tile(5, i-8);
        connect<stream> net1(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(childk[i]) = tile(6, i-16);
        connect<stream> net2(childk[i].out[0], childk[i+32].in[0]);
      }
      else if (i < 32) {
        location<kernel>(childk[i]) = tile(9, i-24);
        connect<stream> net1(childk[i].out[0], childk[i+16].in[0]);
      }
      else if (i < 40) {
        location<kernel>(childk[i]) = tile(12, i-32);
        connect<stream> net1(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 48) {
        location<kernel>(childk[i]) = tile(13, i-40);
        connect<stream> net2(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 56) {
        location<kernel>(childk[i]) = tile(14, i-48);
        if (i == 48)
          connect<stream> net3(childk[i].out[0], childk[56].in[0]);
        if (i == 49)
          connect<stream> net3(childk[i].out[0], childk[56].in[1]);
        if (i == 50)
          connect<stream> net3(childk[i].out[0], childk[57].in[0]);
        if (i == 51)
          connect<stream> net3(childk[i].out[0], childk[57].in[1]);
        if (i == 52)
          connect<stream> net3(childk[i].out[0], childk[58].in[0]);
        if (i == 53)
          connect<stream> net3(childk[i].out[0], childk[58].in[1]);
        if (i == 54)
          connect<stream> net3(childk[i].out[0], childk[59].in[0]);
        if (i == 55)
          connect<stream> net3(childk[i].out[0], childk[59].in[1]);
      }
      else if (i < 58) {
        location<kernel>(childk[i]) = tile(15, i-55);
        if (i == 56)
          connect<stream> net4(childk[i].out[0], childk[60].in[0]);
        if (i == 57)
          connect<stream> net4(childk[i].out[0], childk[60].in[1]);
      }
      else if (i < 60) {
        location<kernel>(childk[i]) = tile(15, i-53);
        if (i == 58)
          connect<stream> net4(childk[i].out[0], childk[61].in[0]);
        if (i == 59)
          connect<stream> net4(childk[i].out[0], childk[61].in[1]);
      }
      else if (i < 62) {
        location<kernel>(childk[i]) = tile(15, i-57);
        if (i == 60)
          connect<stream> net5(childk[i].out[0], rootk.in[0]);
        if (i == 61)
          connect<stream> net5(childk[i].out[0], rootk.in[1]);
      }
    }

    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      
      connect<stream> net20(rootk.out[0], leafk[i].in[0]);

      if (i < 8) {
        location<kernel>(leafk[i]) = tile(0, i);
        connect<stream> net0(leafk[i].out[0], childk[i].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leafk[i]) = tile(1, i-8);
        connect<stream> net0(leafk[i].out[0], childk[i-8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(leafk[i]) = tile(3, i-16);
        connect<stream> net0(leafk[i].out[0], childk[i-8].in[0]);
      }
      else if (i < 32) {
        location<kernel>(leafk[i]) = tile(4, i-24);
        connect<stream> net0(leafk[i].out[0], childk[i-16].in[1]);
      }
      else if (i < 40) {
        location<kernel>(leafk[i]) = tile(7, i-32);
        connect<stream> net0(leafk[i].out[0], childk[i-8].in[0]);
      }
      else if (i < 48) {
        location<kernel>(leafk[i]) = tile(8, i-40);
        connect<stream> net0(leafk[i].out[0], childk[i-16].in[1]);
      }
      else if (i < 56) {
        location<kernel>(leafk[i]) = tile(10, i-48);
        connect<stream> net0(leafk[i].out[0], childk[i-16].in[0]);
      }
      else if (i < 64) {
        location<kernel>(leafk[i]) = tile(11, i-56);
        connect<stream> net0(leafk[i].out[0], childk[i-24].in[1]);
      }
    }
  }
};

