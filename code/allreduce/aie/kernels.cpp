#include <adf.h>
#include "kernels.h"


void root(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * out1, output_stream<uint64> * out2) {
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;
  int32 data = 0;
  time1 = tile.cycles();
  for (int i = 0; i < 10; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    data += data1 + data2;
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out2, time);
  writeincr(out1, data);
}



void child(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < 10; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (data1 + data2));
  }
}



void leaf(input_stream<int32> * in1, output_stream<int32> * out1) {
  for (int32 i = 0; i < 10; i++) {
    writeincr(out1, i);
  }
  int32_t reduce = readincr(in1);
}

