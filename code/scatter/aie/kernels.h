#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void sender(output_stream<int32> *out, uint64 size);
void n1(input_stream<int32> *in, output_stream<int32> *out2, uint64 size);
void n2(input_stream<int32> *in, uint64 size);
void n3(input_stream<int32> *in, uint64 size);
void n4(input_stream<int32> *in, uint64 size);
void n5(input_stream<int32> *in, uint64 size);
void n6(input_stream<int32> *in, uint64 size);
void n7(input_stream<int32> *in, uint64 size);
void n8(input_stream<int32> *in, uint64 size);
void n9(input_stream<int32> *in, uint64 size);
void n10(input_stream<int32> *in, output_stream<int32> *out2, uint64 size);
#endif


