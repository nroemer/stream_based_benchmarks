#include <adf.h>
#include "kernels.h"


void sender(output_stream<int32> *out, uint64 size)
{
  
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data = 7;
  uint64 N = size;

  time1 = tile.cycles();
  for (int32 i = 1; i < 11; i++) {
    for (uint64 j = 0; j < N; j++) {
      writeincr(out, i);
    }
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
}

void n1(input_stream<int32> *in, output_stream<int32> *out2, uint64 size)
{

  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data;
  uint64 N = size;

  time1 = tile.cycles();
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
    writeincr(out2, data);
  }
  for (int i = 0; i < 9*N; i++){
    readincr(in);
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;

}

void n2(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 8*N; i++){
    readincr(in);
  }
}

void n3(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 2*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 7*N; i++){
    readincr(in);
  }
}

void n4(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 3*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 6*N; i++){
    readincr(in);
  }
}

void n5(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 4*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 5*N; i++){
    readincr(in);
  }
}

void n6(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 5*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 4*N; i++){
    readincr(in);
  }
}

void n7(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 6*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 3*N; i++){
    readincr(in);
  }
}

void n8(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 7*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 2*N; i++){
    readincr(in);
  }
}

void n9(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;
  for (int i = 0; i < 8*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  for (int i = 0; i < 1*N; i++){
    readincr(in);
  }
}

void n10(input_stream<int32> *in, output_stream<int32> *out2, uint64 size)
{

  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data;
  uint64 N = size;

  time1 = tile.cycles();
  for (int i = 0; i < 9*N; i++){
    readincr(in);
  }
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
    writeincr(out2, data);
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;

}
