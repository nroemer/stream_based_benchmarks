#include <adf.h>
#include "kernels.h"

using namespace adf;

class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel n1k,n2k,n3k,n4k,n5k,n6k,n7k,n8k,n9k,n10k;

public:
  input_port to_send[11];
  output_gmio gmio_data1, gmio_data10;
  simpleGraph()
  {

    senderk = kernel::create(sender);
    source(senderk) = "kernels.cpp";
    runtime<ratio>(senderk) = 1;
    location<kernel>(senderk) = tile(3, 3);
    connect<parameter>(to_send[0], async(senderk.in[0]));


    n1k = kernel::create(n1);
    source(n1k) = "kernels.cpp";
    runtime<ratio>(n1k) = 1;
    location<kernel>(n1k) = tile(0, 4);
    connect<parameter>(to_send[1], async(n1k.in[1]));



    gmio_data1 = output_gmio::create("gmio_data1", 64, 4000);
    connect<stream>(n1k.out[0], gmio_data1.in[0]);


    n2k = kernel::create(n2);
    source(n2k) = "kernels.cpp";
    runtime<ratio>(n2k) = 1;
    location<kernel>(n2k) = tile(1, 4);
    connect<parameter>(to_send[2], async(n2k.in[1]));

    n3k = kernel::create(n3);
    source(n3k) = "kernels.cpp";
    runtime<ratio>(n3k) = 1;
    location<kernel>(n3k) = tile(2, 4);
    connect<parameter>(to_send[3], async(n3k.in[1]));

    n4k = kernel::create(n4);
    source(n4k) = "kernels.cpp";
    runtime<ratio>(n4k) = 1;
    location<kernel>(n4k) = tile(3, 4);
    connect<parameter>(to_send[4], async(n4k.in[1]));

    n5k = kernel::create(n5);
    source(n5k) = "kernels.cpp";
    runtime<ratio>(n5k) = 1;
    location<kernel>(n5k) = tile(4, 4);
    connect<parameter>(to_send[5], async(n5k.in[1]));

    n6k = kernel::create(n6);
    source(n6k) = "kernels.cpp";
    runtime<ratio>(n6k) = 1;
    location<kernel>(n6k) = tile(5, 4);
    connect<parameter>(to_send[6], async(n6k.in[1]));

    n7k = kernel::create(n7);
    source(n7k) = "kernels.cpp";
    runtime<ratio>(n7k) = 1;
    location<kernel>(n7k) = tile(6, 4);
    connect<parameter>(to_send[7], async(n7k.in[1]));

    n8k = kernel::create(n8);
    source(n8k) = "kernels.cpp";
    runtime<ratio>(n8k) = 1;
    location<kernel>(n8k) = tile(3, 2);
    connect<parameter>(to_send[8], async(n8k.in[1]));

    n9k = kernel::create(n9);
    source(n9k) = "kernels.cpp";
    runtime<ratio>(n9k) = 1;
    location<kernel>(n9k) = tile(2, 2);
    connect<parameter>(to_send[9], async(n9k.in[1]));

    n10k = kernel::create(n10);
    source(n10k) = "kernels.cpp";
    runtime<ratio>(n10k) = 1;
    location<kernel>(n10k) = tile(4, 2);
    connect<parameter>(to_send[10], async(n10k.in[1]));    

    //gmio_n10 = output_gmio::create("gmio_n10", 64, 4000);
    //connect<stream>(n10k.out[0], gmio_n10.in[0]);

    gmio_data10 = output_gmio::create("gmio_data10", 64, 4000);
    connect<stream>(n10k.out[0], gmio_data10.in[0]);


    connect<stream>(senderk.out[0], n1k.in[0]);
    connect<stream>(senderk.out[0], n2k.in[0]);
    connect<stream>(senderk.out[0], n3k.in[0]);
    connect<stream>(senderk.out[0], n4k.in[0]);
    connect<stream>(senderk.out[0], n5k.in[0]);
    connect<stream>(senderk.out[0], n6k.in[0]);
    connect<stream>(senderk.out[0], n7k.in[0]);
    connect<stream>(senderk.out[0], n8k.in[0]);
    connect<stream>(senderk.out[0], n9k.in[0]);
    connect<stream>(senderk.out[0], n10k.in[0]);
  }
};


