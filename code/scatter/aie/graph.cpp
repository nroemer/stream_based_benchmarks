
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"


#include <adf.h>
using namespace adf;
simpleGraph mygraph;


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
#endif


const uint64 sizes[1] = {//22] = {
    10
//    2000,         //   8 KB
//    4000,         //  16 KB
//    8000,         //  32 KB
//    16000,        //  64 KB
//    32000,        // 128 KB
//    64000,        // 256 KB
//    128000,       // 512 KB
//    256000,       //   1 MB
//    512000,       //   2 MB
//    1024000,      //   4 MB
//    2048000,      //   8 MB
//    4096000,      //  16 MB
//    8192000,      //  32 MB
//    16384000,     //  64 MB
//    32768000,     // 128 MB
//    65536000,     // 256 MB
//    131072000,    // 512 MB
//    262144000,    //   1 GB
//    524288000,    //   2 GB
//    1048576000,   //   4 GB
//    2097152000,   //   8 GB
//    4194304000,   //  16 GB
};


int main(int argc, char *argv[])
{
  // Setup standard values
  adf::return_code ret;

  // Setup benchmark data file
  std::string csvName = "broadcast.csv";
  std::ofstream ResultFile(csvName);
  ResultFile << "Data,sender,n1,n10" << std::endl;

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
#endif

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  for (int j = 0; j < (sizeof(sizes)/sizeof(uint64)); j++)
  {
    mygraph.init();

    int aies = 11;
    uint64 N = sizes[j];
    for (int i = 0; i < aies; i++) {
      mygraph.update(mygraph.to_send[i], N);
    }

    int32 *n1 = (int32 *)GMIO::malloc(10 * sizeof(int32));    
    int32 *n10 = (int32 *)GMIO::malloc(10 * sizeof(int32));    
    //uint64 *send_one = (uint64 *)GMIO::malloc(sizeof(uint64));    
    //uint64 *rec_1 = (uint64 *)GMIO::malloc(sizeof(uint64));  
    //uint64 *rec_10 = (uint64 *)GMIO::malloc(sizeof(uint64));    


    //mygraph.gmio_send.aie2gm_nb(send_one, sizeof(uint64));
    //mygraph.gmio_n1.aie2gm_nb(rec_1, sizeof(uint64));
    //mygraph.gmio_n10.aie2gm_nb(rec_10, sizeof(uint64));
    mygraph.gmio_data1.aie2gm_nb(n1, 10 * sizeof(int32));
    mygraph.gmio_data10.aie2gm_nb(n10, 10 * sizeof(int32));

    ret = mygraph.run(1);

    if (ret != adf::ok)
    {
      printf("Host: Run failed\n");
      return ret;
    }

    //mygraph.gmio_send.wait();
    //mygraph.gmio_n1.wait();
    //mygraph.gmio_n10.wait();
    mygraph.gmio_data1.wait();
    mygraph.gmio_data10.wait();
    mygraph.wait();


    ret = mygraph.end();
    if (ret != adf::ok)
      printf("host: end failed\n");
    return ret;
    

 
    for (int i = 0; i < N; i++) {
      printf("Receiver 1 - message %i: %i\n", i, n1[i]);
    }

    for (int i = 0; i < N; i++) {
      printf("Receiver 10 - message %i: %i\n", i, n10[i]);
    }

    for (int i = 0; i < N; i++) {
      ResultFile <<  n1[i] << "," << n10[i] << std::endl;
    }
    //ResultFile << sizes[j] * 4 << "," << send_one[0] << "," << rec_1 << "," << rec_10 << std::endl;

    //GMIO::free(send_one);
    //GMIO::free(rec_1);
    //GMIO::free(rec_10);
    GMIO::free(n1);
    GMIO::free(n10);
  }

  ResultFile.close();

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
