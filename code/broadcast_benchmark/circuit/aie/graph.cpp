
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"


#include <adf.h>
using namespace adf;
simpleGraph mygraph;


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
#endif


const uint64 sizes[22] = {
    2000,         //   8 KB
    4000,         //  16 KB
    8000,         //  32 KB
    16000,        //  64 KB
    32000,        // 128 KB
    64000,        // 256 KB
    128000,       // 512 KB
    256000,       //   1 MB
    512000,       //   2 MB
    1024000,      //   4 MB
    2048000,      //   8 MB
    4096000,      //  16 MB
    8192000,      //  32 MB
    16384000,     //  64 MB
    32768000,     // 128 MB
    65536000,     // 256 MB
    131072000,    // 512 MB
    262144000,    //   1 GB
    524288000,    //   2 GB
    1048576000,   //   4 GB
    2097152000,   //   8 GB
    4194304000,   //  16 GB
};


#include <chrono>
#include <ctime>
class Timer
{
  std::chrono::steady_clock::time_point mTimeStart;

public:
  Timer() { reset(); }
  unsigned long long stop()
  {
    std::chrono::steady_clock::time_point timeEnd =
        std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds> (timeEnd -
                                                                 mTimeStart)
        .count();
  }
  void reset() { mTimeStart = std::chrono::steady_clock::now(); }
};


int main(int argc, char *argv[])
{
  // Setup standard values
  adf::return_code ret;

  // Setup benchmark data file
  std::string csvName = "broadcast.csv";
  std::ofstream ResultFile(csvName);
  ResultFile << "Data,worst,best,median" << std::endl;

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
#endif

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  for (int j = 0; j < (sizeof(sizes)/sizeof(uint64)); j++)
  {
    mygraph.init();

    int aies = 399;
    uint64 N = sizes[j];
    mygraph.update(mygraph.to_send, N);
    for (int i = 0; i < aies; i++) {
      mygraph.update(mygraph.to_receive[i], N);
    }

    uint64 *send = (uint64 *)GMIO::malloc(500 * sizeof(uint64));    
    uint64 *rec = (uint64 *)GMIO::malloc(500 * sizeof(uint64));    

    for (int i = 0; i < 500; i++) {
 
      uint64 *send_one = (uint64 *)GMIO::malloc(sizeof(uint64));    
      uint64 *rec_one = (uint64 *)GMIO::malloc(sizeof(uint64));    

      mygraph.gmio_send.aie2gm_nb(send_one, sizeof(uint64));
      mygraph.gmio_rec.aie2gm_nb(rec_one, sizeof(uint64));

      ret = mygraph.run(1);

      if (ret != adf::ok)
      {
        printf("Host: Run failed\n");
        return ret;
      }

      mygraph.gmio_send.wait();
      mygraph.gmio_rec.wait();
      mygraph.wait();

      send[i] = send_one[0];
      rec[i] = rec_one[0];
      GMIO::free(send_one);
      GMIO::free(rec_one);

    }
    ret = mygraph.end();
    if (ret != adf::ok) {
      printf("host: end failed\n");
    return ret;
    }

    worst = rec[0];
    best = rec[0];
    median = 0.0;
    uint64_t array [500];
  
    for (int i = 0; i < redos; i++) {
      array[i] = rec[i];
      worst = rec[i] > worst ? rec[i] : worst;
      best = rec[i] > best ? best : rec[i];
    }
  
    int n = sizeof(array) / sizeof(array[0]);
    std::sort(array, array + n);
    if (n % 2 != 0) {
      median = array[n/2];
    }
    else {
      median = (array[(n-1)/2] + array[n/2])/2.0;
    }
  
    fprintf(pFile, "%llu,%f,%f,%f\n", (message_size * 4), worst, best, median);
    
    
    GMIO::free(send);
    GMIO::free(rec);
  }

  ResultFile.close();

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
