#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nAIE 399

// This is a basic graph that contains one sending kernel and nAIE receiving kernels
class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel receiverk[nAIE];

public:
  input_port to_send;
  input_port to_receive[nAIE];
  output_gmio gmio_send, gmio_rec;
  simpleGraph()
  {

    // Set up the sender
    senderk = kernel::create(sender);
    source(senderk) = "kernels.cpp";
    runtime<ratio>(senderk) = 1;
    location<kernel>(senderk) = tile(0, 0);
    connect<parameter>(to_send, async(senderk.in[0]));

    gmio_send = output_gmio::create("gmio_send", 64, 4000);
    connect<stream>(senderk.out[1], gmio_send.in[0]);

    // Setup the nAIE receivers
    for (int i = 0; i < nAIE; i++)
    {
      if (i == 49) {
        receiverk[i] = kernel::create(special_receiver);
      }
      else {
        receiverk[i] = kernel::create(receiver);
      }

      connect<stream> net0(senderk.out[0], receiverk[i].in[0]);
      source(receiverk[i]) = "kernels.cpp";
      runtime<ratio>(receiverk[i]) = 1;
      connect<parameter>(to_receive[i], async(receiverk[i].in[1]));

      if (i < 49) {
        location<kernel>(receiverk[i]) = tile(i, 7);
      }
      else if (i < 50) {
        gmio_rec = output_gmio::create("gmio_rec", 64, 4000);
        connect<stream>(receiverk[i].out[0], gmio_rec.in[0]);

        location<kernel>(receiverk[i]) = tile(i, 7);
      }
      else if (i < 100) {
        location<kernel>(receiverk[i]) = tile(i % 50, 6);
      }
      else if (i < 150) {
        location<kernel>(receiverk[i]) = tile(i % 50, 5);
      }
      else if (i < 200) {
        location<kernel>(receiverk[i]) = tile(i % 50, 4);
      }
      else if (i < 250) {
        location<kernel>(receiverk[i]) = tile(i % 50, 3);
      }
      else if (i < 300) {
        location<kernel>(receiverk[i]) = tile(i % 50, 2);
      }
      else if (i < 350) {
        location<kernel>(receiverk[i]) = tile(i % 50, 1);
      }
      else if (i < 399) {
        location<kernel>(receiverk[i]) = tile(i % 50 + 1, 0);
      }
    }
  }
};


