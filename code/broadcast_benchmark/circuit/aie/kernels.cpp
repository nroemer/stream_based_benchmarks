#include <adf.h>
#include "kernels.h"

/**
#define N 100000000000
*/

void sender(output_stream<int32> *out, output_stream<uint64> *out2, uint64 size)
{
  
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data = 7;
  uint64 N = size;

  time1 = tile.cycles();
  for (uint64 i = 0; i < N; i++) {
    writeincr(out, data);
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out2, time);
}


void receiver(input_stream<int32> *in, uint64 size)
{
  int32 data;
  uint64 N = size;

  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
}


void special_receiver(input_stream<int32> *in, output_stream<uint64> *out, uint64 size)
{

  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data;
  uint64 N = size;

  time1 = tile.cycles();
  for (uint64 i = 0; i < N; i++) {
    data = readincr(in);
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out, time);

}
