#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void sender(output_stream<int32> *out, output_stream<uint64> *out2, uint64 size);
void receiver(input_stream<int32> *in, uint64 size);
void special_receiver(input_stream<int32> *in, output_stream<uint64> *out, uint64 size);

#endif


