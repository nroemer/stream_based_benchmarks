# Broadcast Benchmark using packet switching

This code implements a 1-to-N (with N=399) communication and measures the bandwidth of the communication.

## Configuration

The code was developed compiled with the 2022.1 v++ compiler for the xilinx_vck190_base_dfx_202210_1/xilinx_vck190_base_dfx_202210_1.xpfm base platform.


## Directory structure

- the `aie` folder contains the AI-Engine source files (kernels and graph)
- the `sw` folder contains two programs that are used for sw_ and hw_emulation + the xrt.ini configuration file
- the `Makefile` is taking care of the whole compilation process

## Simulation of the program

The code can be tested in two different simulations.

### AIE Simulation

To compile the program for simulation with the aie simulator the command
```
make all TARGET=sw_emu
```
is used.
To automatically start the simulation use
```
make all run_emu TARGET=sw_emu
```

### X86 Simulator

To compile the program for simulation with the x86 simulator the command
```
make all TARGET=hw_emu
```
is used.
To automatically start the simulation use
```
make all run_emu TARGET=hw_emu
```

## Running in HW

To compile the program to run in hardware the command
```
make all TARGET=hw
```
is used.

After the placements of the AIEs are compiled the initiation intervals of the for loops can be found in `Work/aie/placement_of_the_AIE/placement_of_the_AIE.log`.
The placement of the AIEs can be viewed with the `vitis_analyzer`. For this `vitis_analyzer Work/graph.aiecompile_summary` opens a GUI.