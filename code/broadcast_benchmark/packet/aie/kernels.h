#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void sender(output_pktstream *out, uint64 size);
void receiver(input_pktstream *in, uint64 size);
#endif


