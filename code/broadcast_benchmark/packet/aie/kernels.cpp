#include <adf.h>
#include "kernels.h"

/**
#define N 100000000000
*/

const uint32 pktType = 0;

void sender(output_pktstream *out, uint64 size)
{
  uint64 N = size;

  uint32 ID = getPacketid(out, 0);
  writeHeader(out, pktType, ID);

  for (int32 i = 0; i < N; i++) {
    writeincr(out, i);
  }
}


void receiver(input_pktstream *in, uint64 size)
{
  int32 data;
  uint64 N = size;

  readincr(in);

  for (int32 i = 0; i < N; i++) {
    data = readincr(in);
    printf("%i\n", data);
  }
}
