#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nAIE 399

// This is a basic graph that contains one sending kernel and nAIE receiving kernels
class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel receiverk[nAIE];

public:
  input_port to_send;
  input_port to_receive[nAIE];
  simpleGraph()
  {

    // Set up the sender
    senderk = kernel::create(sender);
    source(senderk) = "kernels.cpp";
    runtime<ratio>(senderk) = 1;
    location<kernel>(senderk) = tile(0, 0);
    connect<parameter>(to_send, async(senderk.in[0]));


    // Setup the nAIE receivers
    for (int i = 0; i < nAIE; i++) {
      receiverk[i] = kernel::create(receiver);
      connect<pktstream> (senderk.out[0], receiverk[i].in[0]);
      source(receiverk[i]) = "kernels.cpp";
      runtime<ratio>(receiverk[i]) = 1;
      connect<parameter>(to_receive[i], async(receiverk[i].in[1]));

      if (i < 50) {
        location<kernel>(receiverk[i]) = tile(i, 7);
      }
      else if (i < 100) {
        location<kernel>(receiverk[i]) = tile(i % 50, 6);
      }
      else if (i < 150) {
        location<kernel>(receiverk[i]) = tile(i % 50, 5);
      }
      else if (i < 200) {
        location<kernel>(receiverk[i]) = tile(i % 50, 4);
      }
      else if (i < 250) {
        location<kernel>(receiverk[i]) = tile(i % 50, 3);
      }
      else if (i < 300) {
        location<kernel>(receiverk[i]) = tile(i % 50, 2);
      }
      else if (i < 350) {
        location<kernel>(receiverk[i]) = tile(i % 50, 1);
      }
      else if (i < 399) {
        location<kernel>(receiverk[i]) = tile(i % 50 + 1, 0);
      }
    }
  }
};


