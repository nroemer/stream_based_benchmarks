#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nchilds 2
#define nleafs 4
#define nlcs 28


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel childk[nchilds];
  kernel leafk[nleafs];
  kernel leaf_childk[nlcs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(8, 6);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);

    for (int i = 0; i < nchilds; i++) {
      childk[i] = kernel::create(child);
      source(childk[i]) = "kernels.cpp";
      runtime<ratio>(childk[i]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 1) {
        location<kernel>(childk[i]) = tile(8, 5);
        connect<stream> net5(childk[i].out[0], rootk.in[0]);
      }
      else if (i < 2) {
        location<kernel>(childk[i]) = tile(8, 7);
        connect<stream> net5(childk[i].out[0], rootk.in[1]);
      }
    }
  

    for (int i = 0; i < nlcs; i++) {
      leaf_childk[i] = kernel::create(leaf_child);
      source(leaf_childk[i]) = "kernels.cpp";
      runtime<ratio>(leaf_childk[i]) = 1;
    }
    for (int i = 0; i < nlcs; i++) {
      if (i < 4) {
        location<kernel>(leaf_childk[i]) = tile(1, i+4);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 8) {
        location<kernel>(leaf_childk[i]) = tile(2, i);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 12) {
        location<kernel>(leaf_childk[i]) = tile(3, i-4);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leaf_childk[i]) = tile(4, i-8);
        connect<stream> net3(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 20) {
        location<kernel>(leaf_childk[i]) = tile(5, i-12);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 24) {
        location<kernel>(leaf_childk[i]) = tile(6, i-16);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+4].in[0]);
      }
      else if (i < 28) {
        location<kernel>(leaf_childk[i]) = tile(7, i-20);
        if (i == 24)
          connect<stream> net4(leaf_childk[i].out[0], childk[0].in[0]);
        if (i == 25)
          connect<stream> net4(leaf_childk[i].out[0], childk[0].in[1]);
        if (i == 26)
          connect<stream> net4(leaf_childk[i].out[0], childk[1].in[0]);
        if (i == 27)
          connect<stream> net4(leaf_childk[i].out[0], childk[1].in[1]);
      }
    }


    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      location<kernel>(leafk[i]) = tile(0, i+4);
      connect<stream> net1(leafk[i].out[0], leaf_childk[i].in[0]);
    }
  }
};

