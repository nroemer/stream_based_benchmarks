#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void root(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<uint64> * out1);
void child(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void leaf(output_stream<int32> * out1);
void n0(output_stream<int32> * out1);
void n1(output_stream<int32> * out1);
void n2(output_stream<int32> * out1);
void n3(output_stream<int32> * out1);
void n4(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n5(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n6(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n7(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n8(output_stream<int32> * out1);
void n9(output_stream<int32> * out1);
void n10(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n11(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n12(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n13(input_stream<int32> * restrict in1, output_stream<int32> * restrict out1);
void n14(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
void n15(output_stream<int32> * out1);
void n16(output_stream<int32> * out1);
void n17(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1);
#endif
