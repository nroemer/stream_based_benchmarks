#include <adf.h>
#include "kernels.h"

#define N 1

void root(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<uint64> * out1) {
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;
  int32_t data = 0;

  time1 = tile.cycles();
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    data += 1 + data1 + data2;
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out1, time);

}



void child(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}



void leaf(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n0(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n1(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n2(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n3(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n4(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n5(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n6(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n7(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}



void n8(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n9(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n10(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n11(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n12(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}


void n13(input_stream<int32> * restrict in1, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, (1 + data1));
  }
}


void n14(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}



void n15(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n16(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++) {
    writeincr(out1, 1);
  }
}


void n17(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data1 = READINCR(SS_rsrc1, in1);
    int32_t data2 = READINCR(SS_rsrc2, in2);
    WRITEINCR(MS_rsrc1, out1, (1 + data1 + data2));
  }
}



