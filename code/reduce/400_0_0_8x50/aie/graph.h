#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nchilds 63
#define nleafs 64
#define nother 18


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel childk[3 * nchilds];
  kernel leafk[3 * nleafs];
  kernel otherk[nother];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(17, 7);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream>(rootk.out[0], gmio_time.in[0]);


    // otherk initialization
    otherk[0] = kernel::create(n0);
    location<kernel>(otherk[0]) = tile(15, 0);

    otherk[1] = kernel::create(n1);
    location<kernel>(otherk[1]) = tile(33, 0);

    otherk[2] = kernel::create(n2);
    location<kernel>(otherk[2]) = tile(34, 0);

    otherk[3] = kernel::create(n3);
    location<kernel>(otherk[3]) = tile(16, 0);

    otherk[4] = kernel::create(n4);
    location<kernel>(otherk[4]) = tile(16, 1);

    otherk[5] = kernel::create(n5);
    location<kernel>(otherk[5]) = tile(16, 2);

    otherk[6] = kernel::create(n6);
    location<kernel>(otherk[6]) = tile(16, 3);

    otherk[7] = kernel::create(n7);
    location<kernel>(otherk[7]) = tile(16, 4);

    otherk[8] = kernel::create(n8);
    location<kernel>(otherk[8]) = tile(16, 5);

    otherk[9] = kernel::create(n9);
    location<kernel>(otherk[9]) = tile(16, 6);

    otherk[10] = kernel::create(n10);
    location<kernel>(otherk[10]) = tile(16, 7);

    otherk[11] = kernel::create(n11);
    location<kernel>(otherk[11]) = tile(17, 0);

    otherk[12] = kernel::create(n12);
    location<kernel>(otherk[12]) = tile(17, 1);

    otherk[13] = kernel::create(n13);
    location<kernel>(otherk[13]) = tile(17, 2);

    otherk[14] = kernel::create(n14);
    location<kernel>(otherk[14]) = tile(17, 3);

    otherk[15] = kernel::create(n15);
    location<kernel>(otherk[15]) = tile(17, 4);

    otherk[16] = kernel::create(n16);
    location<kernel>(otherk[16]) = tile(17, 5);

    otherk[17] = kernel::create(n17);
    location<kernel>(otherk[17]) = tile(17, 6);

    for (int i = 0; i < nother; i++) {
      source(otherk[i]) = "kernels.cpp";
      runtime<ratio>(otherk[i]) = 1;
    }




    // first 127_0_0_8x16
    for (int i = 0; i < nchilds; i++) {
      childk[i] = kernel::create(child);
      source(childk[i]) = "kernels.cpp";
      runtime<ratio>(childk[i]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 8) {
        location<kernel>(childk[i]) = tile(2, i);
        connect<stream>(childk[i].out[0], childk[i+16].in[0]);
      }
      else if (i < 16) {
        location<kernel>(childk[i]) = tile(5, i-8);
        connect<stream>(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(childk[i]) = tile(6, i-16);
        connect<stream>(childk[i].out[0], childk[i+32].in[0]);
      }
      else if (i < 32) {
        location<kernel>(childk[i]) = tile(9, i-24);
        connect<stream>(childk[i].out[0], childk[i+16].in[0]);
      }
      else if (i < 40) {
        location<kernel>(childk[i]) = tile(12, i-32);
        connect<stream>(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 48) {
        location<kernel>(childk[i]) = tile(13, i-40);
        connect<stream>(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 56) {
        location<kernel>(childk[i]) = tile(14, i-48);
        if (i == 48)
          connect<stream>(childk[i].out[0], childk[56].in[0]);
        if (i == 49)
          connect<stream>(childk[i].out[0], childk[56].in[1]);
        if (i == 50)
          connect<stream>(childk[i].out[0], childk[57].in[0]);
        if (i == 51)
          connect<stream>(childk[i].out[0], childk[57].in[1]);
        if (i == 52)
          connect<stream>(childk[i].out[0], childk[58].in[0]);
        if (i == 53)
          connect<stream>(childk[i].out[0], childk[58].in[1]);
        if (i == 54)
          connect<stream>(childk[i].out[0], childk[59].in[0]);
        if (i == 55)
          connect<stream>(childk[i].out[0], childk[59].in[1]);
      }
      else if (i < 58) {
        if (i == 56) {
          location<kernel>(childk[i]) = tile(15, 1);
          connect<stream>(childk[i].out[0], childk[60].in[0]);
        }
        if (i == 57) {
          location<kernel>(childk[i]) = tile(15, 3);
          connect<stream>(childk[i].out[0], childk[60].in[1]);
        }
      }
      else if (i < 60) {
        if (i == 58) {
          location<kernel>(childk[i]) = tile(15, 5);
          connect<stream>(childk[i].out[0], childk[61].in[0]);
        }
        if (i == 59) {
          location<kernel>(childk[i]) = tile(15, 7);
          connect<stream>(childk[i].out[0], childk[61].in[1]);
        }
      }
      else if (i < 62) {
        if (i == 60) {
          location<kernel>(childk[i]) = tile(15, 2);
          connect<stream>(childk[i].out[0], childk[62].in[0]);
        }
        if (i == 61) {
          location<kernel>(childk[i]) = tile(15, 6);
          connect<stream>(childk[i].out[0], childk[62].in[1]);
        }
      }
      else if (i < 63) {
        location<kernel>(childk[i]) = tile(15, 4);
        connect<stream>(childk[i].out[0], otherk[10].in[0]);
      }
    }

    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      if (i < 8) {
        location<kernel>(leafk[i]) = tile(0, i);
        connect<stream>(leafk[i].out[0], childk[i].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leafk[i]) = tile(1, i-8);
        connect<stream>(leafk[i].out[0], childk[i-8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(leafk[i]) = tile(3, i-16);
        connect<stream>(leafk[i].out[0], childk[i-8].in[0]);
      }
      else if (i < 32) {
        location<kernel>(leafk[i]) = tile(4, i-24);
        connect<stream>(leafk[i].out[0], childk[i-16].in[1]);
      }
      else if (i < 40) {
        location<kernel>(leafk[i]) = tile(7, i-32);
        connect<stream>(leafk[i].out[0], childk[i-8].in[0]);
      }
      else if (i < 48) {
        location<kernel>(leafk[i]) = tile(8, i-40);
        connect<stream>(leafk[i].out[0], childk[i-16].in[1]);
      }
      else if (i < 56) {
        location<kernel>(leafk[i]) = tile(10, i-48);
        connect<stream>(leafk[i].out[0], childk[i-16].in[0]);
      }
      else if (i < 64) {
        location<kernel>(leafk[i]) = tile(11, i-56);
        connect<stream>(leafk[i].out[0], childk[i-24].in[1]);
      }
    }



    // second 127_0_0_8x16
    for (int i = 0; i < nchilds; i++) {
      childk[i + nchilds] = kernel::create(child);
      source(childk[i + nchilds]) = "kernels.cpp";
      runtime<ratio>(childk[i + nchilds]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 8) {
        location<kernel>(childk[i + nchilds]) = tile(2 + 18, i);
        connect<stream>(childk[i + nchilds].out[0], childk[i+16 + nchilds].in[0]);
      }
      else if (i < 16) {
        location<kernel>(childk[i + nchilds]) = tile(5 + 18, i-8);
        connect<stream>(childk[i + nchilds].out[0], childk[i+8 + nchilds].in[1]);
      }
      else if (i < 24) {
        location<kernel>(childk[i + nchilds]) = tile(6 + 18, i-16);
        connect<stream>(childk[i + nchilds].out[0], childk[i+32 + nchilds].in[0]);
      }
      else if (i < 32) {
        location<kernel>(childk[i + nchilds]) = tile(9 + 18, i-24);
        connect<stream>(childk[i + nchilds].out[0], childk[i+16 + nchilds].in[0]);
      }
      else if (i < 40) {
        location<kernel>(childk[i + nchilds]) = tile(12 + 18, i-32);
        connect<stream>(childk[i + nchilds].out[0], childk[i+8 + nchilds].in[1]);
      }
      else if (i < 48) {
        location<kernel>(childk[i + nchilds]) = tile(13 + 18, i-40);
        connect<stream>(childk[i + nchilds].out[0], childk[i+8 + nchilds].in[1]);
      }
      else if (i < 56) {
        location<kernel>(childk[i + nchilds]) = tile(14 + 18, i-48);
        if (i == 48)
          connect<stream>(childk[i + nchilds].out[0], childk[56 + nchilds].in[0]);
        if (i == 49)
          connect<stream>(childk[i + nchilds].out[0], childk[56 + nchilds].in[1]);
        if (i == 50)
          connect<stream>(childk[i + nchilds].out[0], childk[57 + nchilds].in[0]);
        if (i == 51)
          connect<stream>(childk[i + nchilds].out[0], childk[57 + nchilds].in[1]);
        if (i == 52)
          connect<stream>(childk[i + nchilds].out[0], childk[58 + nchilds].in[0]);
        if (i == 53)
          connect<stream>(childk[i + nchilds].out[0], childk[58 + nchilds].in[1]);
        if (i == 54)
          connect<stream>(childk[i + nchilds].out[0], childk[59 + nchilds].in[0]);
        if (i == 55)
          connect<stream>(childk[i + nchilds].out[0], childk[59 + nchilds].in[1]);
      }
      else if (i < 58) {
        if (i == 56) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 1);
          connect<stream>(childk[i + nchilds].out[0], childk[60 + nchilds].in[0]);
        }
        if (i == 57) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 3);
          connect<stream>(childk[i + nchilds].out[0], childk[60 + nchilds].in[1]);
        }
      }
      else if (i < 60) {
        if (i == 58) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 5);
          connect<stream>(childk[i + nchilds].out[0], childk[61 + nchilds].in[0]);
        }
        if (i == 59) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 7);
          connect<stream>(childk[i + nchilds].out[0], childk[61 + nchilds].in[1]);
        }
      }
      else if (i < 62) {
        if (i == 60) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 2);
          connect<stream>(childk[i + nchilds].out[0], childk[62 + nchilds].in[0]);
        }
        if (i == 61) {
          location<kernel>(childk[i + nchilds]) = tile(15 + 18, 6);
          connect<stream>(childk[i + nchilds].out[0], childk[62 + nchilds].in[1]);
        }
      }
      else if (i < 63) {
        location<kernel>(childk[i + nchilds]) = tile(15 + 18, 4);
        connect<stream>(childk[i + nchilds].out[0], otherk[17].in[0]);
      }
    }

    for (int i = 0; i < nleafs; i++)  {
      leafk[i + nleafs] = kernel::create(leaf);
      source(leafk[i + nleafs]) = "kernels.cpp";
      runtime<ratio>(leafk[i + nleafs]) = 1;
      if (i < 8) {
        location<kernel>(leafk[i + nleafs]) = tile(0 + 18, i);
        connect<stream>(leafk[i + nleafs].out[0], childk[i+ nchilds].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leafk[i + nleafs]) = tile(1 + 18, i-8);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-8+ nchilds].in[1]);
      }
      else if (i < 24) {
        location<kernel>(leafk[i + nleafs]) = tile(3 + 18, i-16);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-8+ nchilds].in[0]);
      }
      else if (i < 32) {
        location<kernel>(leafk[i + nleafs]) = tile(4 + 18, i-24);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-16+ nchilds].in[1]);
      }
      else if (i < 40) {
        location<kernel>(leafk[i + nleafs]) = tile(7 + 18, i-32);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-8+ nchilds].in[0]);
      }
      else if (i < 48) {
        location<kernel>(leafk[i + nleafs]) = tile(8 + 18, i-40);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-16+ nchilds].in[1]);
      }
      else if (i < 56) {
        location<kernel>(leafk[i + nleafs]) = tile(10 + 18, i-48);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-16+ nchilds].in[0]);
      }
      else if (i < 64) {
        location<kernel>(leafk[i + nleafs]) = tile(11 + 18, i-56);
        connect<stream>(leafk[i + nleafs].out[0], childk[i-24+ nchilds].in[1]);
      }
    }


    // third 127_0_0_8x16
    for (int i = 0; i < nchilds; i++) {
      childk[i + nchilds + nchilds] = kernel::create(child);
      source(childk[i + nchilds + nchilds]) = "kernels.cpp";
      runtime<ratio>(childk[i + nchilds + nchilds]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 8) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 2, i);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+16 + nchilds + nchilds].in[0]);
      }
      else if (i < 16) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 5, i-8);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+8 + nchilds + nchilds].in[1]);
      }
      else if (i < 24) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 6, i-16);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+32 + nchilds + nchilds].in[0]);
      }
      else if (i < 32) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 9, i-24);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+16 + nchilds + nchilds].in[0]);
      }
      else if (i < 40) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 12, i-32);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+8 + nchilds + nchilds].in[1]);
      }
      else if (i < 48) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 13, i-40);
        connect<stream>(childk[i + nchilds + nchilds].out[0], childk[i+8 + nchilds + nchilds].in[1]);
      }
      else if (i < 56) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 14, i-48);
        if (i == 48)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[56 + nchilds + nchilds].in[0]);
        if (i == 49)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[56 + nchilds + nchilds].in[1]);
        if (i == 50)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[57 + nchilds + nchilds].in[0]);
        if (i == 51)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[57 + nchilds + nchilds].in[1]);
        if (i == 52)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[58 + nchilds + nchilds].in[0]);
        if (i == 53)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[58 + nchilds + nchilds].in[1]);
        if (i == 54)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[59 + nchilds + nchilds].in[0]);
        if (i == 55)
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[59 + nchilds + nchilds].in[1]);
      }
      else if (i < 58) {
        if (i == 56) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 1);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[60 + nchilds + nchilds].in[0]);
        }
        if (i == 57) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 3);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[60 + nchilds + nchilds].in[1]);
        }
      }
      else if (i < 60) {
        if (i == 58) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 5);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[61 + nchilds + nchilds].in[0]);
        }
        if (i == 59) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 7);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[61 + nchilds + nchilds].in[1]);
        }
      }
      else if (i < 62) {
        if (i == 60) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 2);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[62 + nchilds + nchilds].in[0]);
        }
        if (i == 61) {
          location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 6);
          connect<stream>(childk[i + nchilds + nchilds].out[0], childk[62 + nchilds + nchilds].in[1]);
        }
      }
      else if (i < 63) {
        location<kernel>(childk[i + nchilds + nchilds]) = tile(49 - 15, 4);
        connect<stream>(childk[i + nchilds + nchilds].out[0], otherk[17].in[1]);
      }
    }

    for (int i = 0; i < nleafs; i++)  {
      leafk[i + nleafs + nleafs] = kernel::create(leaf);
      source(leafk[i + nleafs + nleafs]) = "kernels.cpp";
      runtime<ratio>(leafk[i + nleafs + nleafs]) = 1;
      if (i < 8) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 0, i);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i + nchilds + nchilds].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 1, i-8);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-8 + nchilds + nchilds].in[1]);
      }
      else if (i < 24) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 3, i-16);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-8 + nchilds + nchilds].in[0]);
      }
      else if (i < 32) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 4, i-24);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-16 + nchilds + nchilds].in[1]);
      }
      else if (i < 40) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 7, i-32);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-8 + nchilds + nchilds].in[0]);
      }
      else if (i < 48) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 8, i-40);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-16 + nchilds + nchilds].in[1]);
      }
      else if (i < 56) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 10, i-48);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-16 + nchilds + nchilds].in[0]);
      }
      else if (i < 64) {
        location<kernel>(leafk[i + nleafs + nleafs]) = tile(49 - 11, i-56);
        connect<stream>(leafk[i + nleafs + nleafs].out[0], childk[i-24 + nchilds + nchilds].in[1]);
      }
    }
    

    
    // otherk connections
    connect<stream>(otherk[0].out[0], otherk[4].in[0]);
    connect<stream>(otherk[1].out[0], otherk[11].in[0]);
    connect<stream>(otherk[2].out[0], otherk[11].in[1]);
    connect<stream>(otherk[3].out[0], otherk[4].in[1]);
    connect<stream>(otherk[4].out[0], otherk[5].in[0]);
    connect<stream>(otherk[5].out[0], otherk[6].in[0]);
    connect<stream>(otherk[6].out[0], otherk[10].in[1]);
    connect<stream>(otherk[7].out[0], otherk[5].in[1]);
    connect<stream>(otherk[8].out[0], otherk[7].in[0]);
    connect<stream>(otherk[9].out[0], otherk[7].in[1]);
    connect<stream>(otherk[10].out[0], rootk.in[0]);
    connect<stream>(otherk[11].out[0], otherk[12].in[0]);
    connect<stream>(otherk[12].out[0], otherk[6].in[1]);
    connect<stream>(otherk[13].out[0], otherk[12].in[1]);
    connect<stream>(otherk[14].out[0], otherk[13].in[0]);
    connect<stream>(otherk[15].out[0], otherk[14].in[0]);
    connect<stream>(otherk[16].out[0], otherk[14].in[1]);
    connect<stream>(otherk[17].out[0], rootk.in[1]);
  }
};

