#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <adf.h>
using namespace adf;
simpleGraph mygraph;

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
#endif


int main(int argc, char *argv[])
{
  adf::return_code ret;

  // Setup benchmark data file
  FILE * pFile;
  pFile = fopen("red.csv", "w");
  fprintf(pFile, "cycles\n");


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif


  uint64_t out_size = sizeof(uint64_t);
  
  uint64 *time = (uint64 *)GMIO::malloc(1000 * out_size);
  uint64 *tmp = (uint64 *)GMIO::malloc(out_size);

  for (int i = 0; i < 1000; i++) {
    mygraph.gmio_time.aie2gm_nb(tmp, out_size);

    mygraph.init();

    ret = mygraph.run(1);
    if (ret != adf::ok){
      printf("Host: Run failed\n");
      return ret;
    }

    mygraph.gmio_time.wait();

    mygraph.wait();
  
    time[i] = tmp[0];
  }

  ret = mygraph.end();
  if (ret != adf::ok) {
    printf("Host: End failed\n");
    return ret;
  }

  uint64_t res = 0;
  uint64_t array [1000];
  for (int i = 0; i < 1000; i++) {
    array[i] = time[i];
  }

  int n = sizeof(array) / sizeof(array[0]);
  std::sort(array, array + n);
  if (n % 2 != 0) {
    res = array[n/2];
  }
  else {
    res = (array[(n-1)/2] + array[n/2])/2.0;
  }

  fprintf(pFile, "%lu", res);

  GMIO::free(time);
  GMIO::free(tmp);

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}

