#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nleafs 2
#define nlcs 30


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel leafk[nleafs];
  kernel leaf_childk[nlcs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(16, 7);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);


    for (int i = 0; i < nlcs; i++) {
      leaf_childk[i] = kernel::create(leaf_child);
      source(leaf_childk[i]) = "kernels.cpp";
      runtime<ratio>(leaf_childk[i]) = 1;
    }
    for (int i = 0; i < nlcs; i++) {
      if (i < 2) {
        location<kernel>(leaf_childk[i]) = tile(1, i+6);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 4) {
        location<kernel>(leaf_childk[i]) = tile(2, i+4);
        connect<stream> net3(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 6) {
        location<kernel>(leaf_childk[i]) = tile(3, i+2);
        connect<stream> net4(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 8) {
        location<kernel>(leaf_childk[i]) = tile(4, i);
        connect<stream> net5(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 10) {
        location<kernel>(leaf_childk[i]) = tile(5, i-2);
        connect<stream> net6(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 12) {
        location<kernel>(leaf_childk[i]) = tile(6, i-4);
        connect<stream> net7(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 14) {
        location<kernel>(leaf_childk[i]) = tile(7, i-6);
        connect<stream> net8(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leaf_childk[i]) = tile(8, i-8);
        connect<stream> net9(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 18) {
        location<kernel>(leaf_childk[i]) = tile(9, i-10);
        connect<stream> net10(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 20) {
        location<kernel>(leaf_childk[i]) = tile(10, i-12);
        connect<stream> net11(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 22) {
        location<kernel>(leaf_childk[i]) = tile(11, i-14);
        connect<stream> net12(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 24) {
        location<kernel>(leaf_childk[i]) = tile(12, i-16);
        connect<stream> net13(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 26) {
        location<kernel>(leaf_childk[i]) = tile(13, i-18);
        connect<stream> net14(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 28) {
        location<kernel>(leaf_childk[i]) = tile(14, i-20);
        connect<stream> net15(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 30) {
        location<kernel>(leaf_childk[i]) = tile(15, i-22);
        if (i == 28)
          connect<stream> net16(leaf_childk[i].out[0], rootk.in[0]);
        if (i == 29)
          connect<stream> net16(leaf_childk[i].out[0], rootk.in[1]);
      }
    }


    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      location<kernel>(leafk[i]) = tile(0, i+6);
      connect<stream> net1(leafk[i].out[0], leaf_childk[i].in[0]);
    }
  }
};

