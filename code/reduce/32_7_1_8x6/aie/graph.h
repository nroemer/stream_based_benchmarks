#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nchilds 6
#define nleafs 8
#define nlcs 24


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel childk[nchilds];
  kernel leafk[nleafs];
  kernel leaf_childk[nlcs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(5, 3);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);

    for (int i = 0; i < nchilds; i++) {
      childk[i] = kernel::create(child);
      source(childk[i]) = "kernels.cpp";
      runtime<ratio>(childk[i]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 2) {
        location<kernel>(childk[i]) = tile(4, i+1);
        if (i == 0)
          connect<stream> net5(childk[i].out[0], childk[4].in[0]);
        if (i == 1)
          connect<stream> net5(childk[i].out[0], childk[4].in[1]);
      }
      else if (i < 4) {
        location<kernel>(childk[i]) = tile(4, i+3);
        if (i == 2)
          connect<stream> net5(childk[i].out[0], childk[5].in[0]);
        if (i == 3)
          connect<stream> net5(childk[i].out[0], childk[5].in[1]);
      }
      else if (i < 6) {
        location<kernel>(childk[i]) = tile(4, i-1);
        if (i == 4)
          connect<stream> net6(childk[i].out[0], rootk.in[0]);
        if (i == 5)
          connect<stream> net6(childk[i].out[0], rootk.in[1]);
      }
    }
  

    for (int i = 0; i < nlcs; i++) {
      leaf_childk[i] = kernel::create(leaf_child);
      source(leaf_childk[i]) = "kernels.cpp";
      runtime<ratio>(leaf_childk[i]) = 1;
    }
    for (int i = 0; i < nlcs; i++) {
      if (i < 8) {
        location<kernel>(leaf_childk[i]) = tile(1, i);
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+8].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leaf_childk[i]) = tile(2, i-8);
        connect<stream> net3(leaf_childk[i].out[0], leaf_childk[i+8].in[0]);
      }
      else if (i < 24) {
        location<kernel>(leaf_childk[i]) = tile(3, i-16);
        if (i == 16)
          connect<stream> net4(leaf_childk[i].out[0], childk[0].in[0]);
        if (i == 17)
          connect<stream> net4(leaf_childk[i].out[0], childk[0].in[1]);
        if (i == 18)
          connect<stream> net4(leaf_childk[i].out[0], childk[1].in[0]);
        if (i == 19)
          connect<stream> net4(leaf_childk[i].out[0], childk[1].in[1]);
        if (i == 20)
          connect<stream> net4(leaf_childk[i].out[0], childk[2].in[0]);
        if (i == 21)
          connect<stream> net4(leaf_childk[i].out[0], childk[2].in[1]);
        if (i == 22)
          connect<stream> net4(leaf_childk[i].out[0], childk[3].in[0]);
        if (i == 23)
          connect<stream> net4(leaf_childk[i].out[0], childk[3].in[1]);
      }
    }


    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      location<kernel>(leafk[i]) = tile(0, i);
      connect<stream> net1(leafk[i].out[0], leaf_childk[i].in[0]);
    }
  }
};

