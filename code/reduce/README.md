# Reduce operation using circuit switching

These programs implement a N-to-1 reduce for N in {32,63,64,127,400}.

## Configuration

The code was developed compiled with the 2022.1 v++ compiler for the xilinx_vck190_base_dfx_202210_1/xilinx_vck190_base_dfx_202210_1.xpfm base platform.


## Directory structure

The folders follow the same naming schema as the implementations in the thesis.
The naming schema is:
```
a_b_c_dxe
```
- a is the number of AIEs contributing data to the reduce,
- b is the number of AIEs not contributing data to the reduce but are involved in the communication,
- c is the minimum number of free input ports on the AIEs contributing data to the reduce - possible values are \{0,1,2\},
- d is the number of rows used in the implementation on the AIE Array - possible values are in [0,7],
- e is the number of columns used in the implementation on the AIE Array - possible values are in [0,49].

In the folders the structure is:
- the `aie` folder contains the AI-Engine source files (kernels and graph)
- the `sw` folder contains two programs that are used for sw_ and hw_emulation + the xrt.ini configuration file
- the `Makefile` is taking care of the whole compilation process

## Simulation of the program

The code can be tested in two different simulations.

### AIE Simulation

To compile the program for simulation with the aie simulator the command
```
make all TARGET=sw_emu
```
is used.
To automatically start the simulation use
```
make all run_emu TARGET=sw_emu
```

### X86 Simulator

To compile the program for simulation with the x86 simulator the command
```
make all TARGET=hw_emu
```
is used.
To automatically start the simulation use
```
make all run_emu TARGET=hw_emu
```

## Running in HW

To compile the program to run in hardware the command
```
make all TARGET=hw
```
is used.

After the placements of the AIEs are compiled the initiation intervals of the for loops can be found in `Work/aie/placement_of_the_AIE/placement_of_the_AIE.log`.
The placement of the AIEs can be viewed with the `vitis_analyzer`. For this `vitis_analyzer Work/graph.aiecompile_summary` opens a GUI.