#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nchilds 30
#define nleafs 32


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel childk[nchilds];
  kernel leafk[nleafs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(8, 3);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);

    for (int i = 0; i < nchilds; i++) {
      childk[i] = kernel::create(child);
      source(childk[i]) = "kernels.cpp";
      runtime<ratio>(childk[i]) = 1;
    }
    for (int i = 0; i < nchilds; i++) {
      if (i < 8) {
        location<kernel>(childk[i]) = tile(2, i);
        connect<stream> net1(childk[i].out[0], childk[i+16].in[0]);
      }
      else if (i < 16) {
        location<kernel>(childk[i]) = tile(5, i-8);
        connect<stream> net1(childk[i].out[0], childk[i+8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(childk[i]) = tile(6, i-16);
        if (i == 16)
          connect<stream> net2(childk[i].out[0], childk[24].in[0]);
        if (i == 17)
          connect<stream> net2(childk[i].out[0], childk[24].in[1]);
        if (i == 18)
          connect<stream> net2(childk[i].out[0], childk[25].in[0]);
        if (i == 19)
          connect<stream> net2(childk[i].out[0], childk[25].in[1]);
        if (i == 20)
          connect<stream> net2(childk[i].out[0], childk[26].in[0]);
        if (i == 21)
          connect<stream> net2(childk[i].out[0], childk[26].in[1]);
        if (i == 22)
          connect<stream> net2(childk[i].out[0], childk[27].in[0]);
        if (i == 23)
          connect<stream> net2(childk[i].out[0], childk[27].in[1]);
      }
      else if (i < 26) {
        location<kernel>(childk[i]) = tile(7, i-23);
        if (i == 24)
          connect<stream> net3(childk[i].out[0], childk[28].in[0]);
        if (i == 25)
          connect<stream> net3(childk[i].out[0], childk[28].in[1]);
      }
      else if (i < 28) {
        location<kernel>(childk[i]) = tile(7, i-21);
        if (i == 26)
          connect<stream> net3(childk[i].out[0], childk[29].in[0]);
        if (i == 27)
          connect<stream> net3(childk[i].out[0], childk[29].in[1]);
      }
      else if (i < 30) {
        location<kernel>(childk[i]) = tile(7, i-25);
        if (i == 28)
          connect<stream> net4(childk[i].out[0], rootk.in[0]);
        if (i == 29)
          connect<stream> net4(childk[i].out[0], rootk.in[1]);
      }
    }

    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      if (i < 8) {
        location<kernel>(leafk[i]) = tile(0, i);
        connect<stream> net0(leafk[i].out[0], childk[i].in[0]);
      }
      else if (i < 16) {
        location<kernel>(leafk[i]) = tile(1, i-8);
        connect<stream> net0(leafk[i].out[0], childk[i-8].in[1]);
      }
      else if (i < 24) {
        location<kernel>(leafk[i]) = tile(3, i-16);
        connect<stream> net0(leafk[i].out[0], childk[i-8].in[0]);
      }
      else if (i < 32) {
        location<kernel>(leafk[i]) = tile(4, i-24);
        connect<stream> net0(leafk[i].out[0], childk[i-16].in[1]);
      }
    }
  }
};

