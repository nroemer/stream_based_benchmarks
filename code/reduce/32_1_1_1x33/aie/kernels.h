#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void root(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<uint64> * out1);
void leaf_child(input_stream<int32> * restrict in1, output_stream<int32> * restrict out1);
void leaf(output_stream<int32> * out1);
#endif
