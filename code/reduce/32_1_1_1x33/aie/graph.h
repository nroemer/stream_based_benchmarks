#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nleafs 2
#define nlcs 30


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel leafk[nleafs];
  kernel leaf_childk[nlcs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(32, 7);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);

    for (int i = 0; i < nlcs; i++) {
      leaf_childk[i] = kernel::create(leaf_child);
      source(leaf_childk[i]) = "kernels.cpp";
      runtime<ratio>(leaf_childk[i]) = 1;
    }
    for (int i = 0; i < nlcs; i++) {
      location<kernel>(leaf_childk[i]) = tile(i+2, 7);
      if (i < 28) {
        connect<stream> net2(leaf_childk[i].out[0], leaf_childk[i+2].in[0]);
      }
      else if (i < 30) {
        if (i == 28)
          connect<stream> net16(leaf_childk[i].out[0], rootk.in[0]);
        if (i == 29)
          connect<stream> net16(leaf_childk[i].out[0], rootk.in[1]);
      }
    }


    for (int i = 0; i < nleafs; i++)  {
      leafk[i] = kernel::create(leaf);
      source(leafk[i]) = "kernels.cpp";
      runtime<ratio>(leafk[i]) = 1;
      location<kernel>(leafk[i]) = tile(i, 7);
      connect<stream> net1(leafk[i].out[0], leaf_childk[i].in[0]);
    }
  }
};

