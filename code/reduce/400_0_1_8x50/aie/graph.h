#include <adf.h>
#include "kernels.h"

using namespace adf;

#define nlcs 398


class simpleGraph : public graph {
private:
  kernel rootk;
  kernel leafk;
  kernel leaf_childk[nlcs];

public:
  output_gmio gmio_time;

  simpleGraph() {
    rootk = kernel::create(root);
    source(rootk) = "kernels.cpp";
    runtime<ratio>(rootk) = 1;
    location<kernel>(rootk) = tile(0, 0);
    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> net200(rootk.out[0], gmio_time.in[0]);

    for (int i = 0; i < nlcs; i++) {
      leaf_childk[i] = kernel::create(leaf_child);
      source(leaf_childk[i]) = "kernels.cpp";
      runtime<ratio>(leaf_childk[i]) = 1;
    }
    for (int i = 0; i < nlcs; i++) {
      if (i < 49) {
        location<kernel>(leaf_childk[i]) = tile(i+1, 7);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 99) {
        location<kernel>(leaf_childk[i]) = tile(98-i, 6);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 149) {
        location<kernel>(leaf_childk[i]) = tile(i-99, 5);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 199) {
        location<kernel>(leaf_childk[i]) = tile(198-i, 4);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 249) {
        location<kernel>(leaf_childk[i]) = tile(i-199, 3);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 299) {
        location<kernel>(leaf_childk[i]) = tile(298-i, 2);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 349) {
        location<kernel>(leaf_childk[i]) = tile(i-299, 1);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 397) {
        location<kernel>(leaf_childk[i]) = tile(398-i, 0);
        connect<stream>(leaf_childk[i].out[0], leaf_childk[i+1].in[0]);
      }
      else if (i < 398) {
        location<kernel>(leaf_childk[i]) = tile(1, 0);
        connect<stream>(leaf_childk[i].out[0], rootk.in[0]);
      }
    }



    leafk = kernel::create(leaf);
    source(leafk) = "kernels.cpp";
    runtime<ratio>(leafk) = 1;
    location<kernel>(leafk) = tile(0, 7);
    connect<stream> net1(leafk.out[0], leaf_childk[0].in[0]);

  }
};

