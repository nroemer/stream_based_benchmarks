F_PROJ_ROOT ?= $(shell bash -c 'export MK_PATH=$(MK_PATH); echo $${MK_PATH%/AI_Engine_Development/*}')

ROOTFS := ${XILINX_VERSAL}/rootfs.ext4
IMAGE := ${XILINX_VERSAL}/Image
# BASE_PLATFORM := ${PLATFORM_REPO_PATHS}/xilinx_vck190_base_202210_1/xilinx_vck190_base_202210_1.xpfm
BASE_PLATFORM := ${PLATFORM_REPO_PATHS}/xilinx_vck190_base_dfx_202210_1/xilinx_vck190_base_dfx_202210_1.xpfm 

SDKTARGETSYSROOT ?= ${SYSROOT}
# to store binaries
TIMESTAMP = $(shell date +'%F-%T')
# Makefile input options
TARGET := sw_emu
PFM := p2p

# File names and locations
GRAPH := aie/graph.cpp aie/kernels.cpp
GRAPH_O := libadf.a
APP := p2p_app


# Command-line options
VPP := v++
AIECC := aiecompiler
AIESIM := aiesimulator
X86SIM := x86simulator
SW_EMU_CMD := ./launch_sw_emu.sh
HW_EMU_CMD := ./launch_hw_emu.sh -add-env AIE_COMPILER_WORKDIR=../Work 

AIE_INCLUDE_FLAGS := -include="$(XILINX_VITIS)/aietools/include" -include="./aie"  -include="./"
AIE_FLAGS := $(AIE_INCLUDE_FLAGS) --platform $(BASE_PLATFORM) -workdir=./Work -v 


ifeq ($(TARGET),sw_emu)
	AIE_FLAGS += --target=x86sim
else
	AIE_FLAGS += --target=hw
endif 

VPP_XO_FLAGS := -c --platform $(BASE_PLATFORM) -t $(TARGET) --save-temps -g
VPP_LINK_FLAGS := -l -t $(TARGET) --platform $(BASE_PLATFORM) $(KERNEL_XO) $(GRAPH_O)  --save-temps -g -o $(PFM).xsa
VPP_FLAGS := $(VPP_LINK_FLAGS)

GCC_FLAGS := -Wall -c \
	     -std=c++14 \
	     -Wno-int-to-pointer-cast \
	     --sysroot=${SDKTARGETSYSROOT}

ifeq ($(TARGET),sw_emu)
	GCC_FLAGS += -D__SYNCBO_ENABLE__ -D__PS_ENABLE_AIE__
endif

GCC_INCLUDES := -I$(SDKTARGETSYSROOT)/usr/include/xrt \
			-I$(SDKTARGETSYSROOT)/usr/include \
			-I./ -I../aie \
			-I${XILINX_VITIS}/aietools/include \
			-I${XILINX_VITIS}/include

ifeq ($(TARGET),sw_emu)
	GCC_LIB := -lxaiengine -ladf_api_xrt -lxrt_coreutil -L$(SDKTARGETSYSROOT)/usr/lib -L${XILINX_VITIS}/aietools/lib/aarch64.o --sysroot=${SDKTARGETSYSROOT}
else
	GCC_LIB := -lxaiengine -ladf_api_xrt -lxrt_core -lxrt_coreutil -L$(SDKTARGETSYSROOT)/usr/lib -L${XILINX_VITIS}/aietools/lib/aarch64.o --sysroot=${SDKTARGETSYSROOT}
endif

LDCLFLAGS := $(GCC_LIB)

.ONESHELL:
.PHONY: clean all kernels aie sim xsa host package run_emu

###
# Guarding Checks. Do not modify.
###
check_defined = \
	$(strip $(foreach 1,$1, \
		$(call __check_defined,$1,$(strip $(value 2)))))

__check_defined = \
	$(if $(value $1),, \
		$(error Undefined $1$(if $2, ($2))))


###

all: clean_all aie xsa p2p_app package # -- all
sd_card: all

######################################################
# This step compiles the HLS C kernels and creates an ADF Graph
# The graph is generated by having the -target=hw

aie: $(GRAPH_O)

#AIE or X86 Simulation
sim: $(GRAPH_O)
     
ifeq ($(TARGET),sw_emu)
	$(X86SIM) --pkg-dir=./Work
else
	$(AIESIM) --profile --dump-vcd=tutorial --pkg-dir=./Work
endif 

#AIE or X86 compilation
$(GRAPH_O): $(GRAPH)
	$(AIECC) $(AIE_FLAGS) $(GRAPH)
	@echo "COMPLETE: libadf.a created."
#####################################################

########################################################
# Once the kernels and graph are generated, you can build
# the hardware part of the design. This creates an xsa
# that will be used to run the design on the platform.
xsa: $(GRAPH_O)
	$(VPP) $(VPP_LINK_FLAGS) || (echo "task: [xsa] failed error code: $$?"; exit 1)
	@echo "COMPLETE: .xsa created."
########################################################

############################################################################################################################
# For hw compile the code generated for PS

$(APP): aie/graph.cpp Work/ps/c_rts/aie_control_xrt.cpp
	mkdir -p sw
	$(CXX) $^ $(GCC_INCLUDES) $(GCC_LIB) -o $@

############################################################################################################################

##################################################################################################
# Depending on the TARGET, it'll either generate the PDI for sw_emu,hw_emu or hw.
# Packaging for the dfx platform.
# Using instructions provided at https://docs.xilinx.com/r/en-US/ug1076-ai-engine-environment/Targeting-the-DFX-Platform
package: $(APP)
ifeq ($(TARGET),hw)
	cd ./sw
	v++ -p -t ${TARGET} \
        -f ${BASE_PLATFORM} \
        --package.defer_aie_run \
        -o a.xclbin \
        ../$(PFM).xsa ../libadf.a
	v++ -p -t ${TARGET} \
		-f ${BASE_PLATFORM} \
		--package.rootfs ${ROOTFS} \
		--package.image_format=ext4 \
		--package.kernel_image ${IMAGE} \
		--package.boot_mode=sd \
		--package.sd_file ../$(APP)\
		--package.sd_file a.xclbin
	@echo "COMPLETE: emulation package created."
endif
ifeq ($(TARGET),sw_emu)
	cd ./sw
	v++ -p -t ${TARGET} \
		-f ${BASE_PLATFORM} \
		--package.rootfs=${ROOTFS} \
		--package.image_format=ext4 \
		--package.boot_mode=sd \
		--package.kernel_image=${IMAGE} \
		--package.defer_aie_run \
		--package.sd_file embedded_exec_sw.sh \
		--package.sd_file ../$(APP) ../$(PFM).xsa ../libadf.a
	@echo "COMPLETE: emulation package created."
endif 
ifeq ($(TARGET),hw_emu)
	cd ./sw
	v++ -p -t ${TARGET} \
		-f ${BASE_PLATFORM} \
		--package.rootfs=${ROOTFS} \
		--package.image_format=ext4 \
		--package.boot_mode=sd \
		--package.kernel_image=${IMAGE} \
		--package.defer_aie_run \
		--package.sd_file embedded_exec.sh \
		--package.sd_file ../$(APP) ../$(PFM).xsa ../libadf.a
	@echo "COMPLETE: emulation package created."
endif

###################################################################################################

#Build the design and then run sw/hw emulation 
run: all run_emu


###########################################################################
copy:
	@echo "INFO:- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "INFO: Archiving files to binaries/$(TIMESTAMP)" 
	mkdir -p ./binaries/$(TIMESTAMP)
	cp ./sw/a.xclbin ./binaries/$(TIMESTAMP)
	cp ./${APP} ./binaries/$(TIMESTAMP)
	cp -r ../aie ./binaries/$(TIMESTAMP)
	@echo "COMPLETE: copied all files"

###########################################################################
run_emu: 
# If the target is for SW_EMU, launch the emulator
ifeq (${TARGET},sw_emu)
	cd ./sw
	$(SW_EMU_CMD)
else
# If the target is for HW_EMU, launch the emulator
ifeq (${TARGET},hw_emu)
	cd ./sw
	$(HW_EMU_CMD)
else
	@echo "Hardware build, no emulation executed."
endif
endif

###########################################################################

clean_all: clean
	rm -rf _x v++* *.o *.compile_summary* $(GRAPH_O) *.xpe xnwOut *.xclbin* *.log *.xsa Work *.db *.csv *.jou .Xil .run .AIE_SIM_CMD_LINE_OPTIONS ISS_RPC_SERVER_PORT pl_sample_counts *.vcd *.link_summary*
	rm -rf sw/qemu* sw/sim vitis_analyzer* sw/pl_script.sh sw/root@versal2_ sw/x86simulator_output .ipcache sw/graph.cpp


clean:
	rm -rf _x v++* $(KERNEL_XO) $(GRAPH_O) *.o *.compile_summary* *.xpe xnwOut *.xclbin* *.log *.xsa Work *.db *.csv *$(PFM)* *.jou .Xil
	rm -rf sw/*.log sw/*.xclbin sw/cfg/ sw/launch_hw_emu.sh sw/qemu_dts_files sw/emu_qemu_scripts sw/*.exe sw/_x/ sw/*summary sw/*.o sw/*.elf sw/*.xpe sw/xnwOut sw/Work sw/*.csv sw/*.db sw/*.bin sw/*.BIN sw/*.bif sw/launch_hw_emulator.sh sw/*.txt sw/emulation sw/.Xil ./x86simulator_output
	rm -rf sw/sd_card sw/sd_card.img sw/*.o ./*.exe sw/qeumu* x86simulator_output/ aiesimulator_output/
	rm -rf sw/*.log sw/*.xclbin sw/cfg/ sw/launch_sw_emu.sh sw/launch_hw_emu.sh sw/qemu_dts_files sw/emu_qemu_scripts sw/*.exe sw/_x/ sw/*summary sw/*.o sw/*.elf sw/*.xpe sw/xnwOut sw/Work sw/*.csv sw/*.db sw/*.bin sw/*.BIN sw/*.bif sw/launch_hw_emulator.sh sw/*.txt sw/emulation sw/.Xil ./x86simulator_output
	rm -rf sw/sd_card sw/sd_card.img sw/*.o ./*.exe sw/qeumu* x86simulator_output/ aiesimulator_output/
	rm -rf *.log

clean_for_x86:
	rm -rf sw/*.log sw/*.xclbin sw/cfg/ sw/launch_hw_emu.sh sw/qemu_dts_files sw/emu_qemu_scripts sw/*.exe sw/_x/ sw/*summary sw/*.o sw/*.elf sw/*.xpe sw/xnwOut sw/Work sw/*.csv sw/*.db sw/*.bin sw/*.BIN sw/*.bif sw/launch_hw_emulator.sh sw/*.txt sw/emulation sw/.Xil
	rm -rf sw/sd_card sw/sd_card.img sw/*.o sw/*.exe sw/qemu* sw/launch_sw_* 

