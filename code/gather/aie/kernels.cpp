#include <adf.h>
#include "kernels.h"

#define N 2


void n1(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 1);
  }
}


void n2(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 2);
  }
}


void n3(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 3);
  }
}


void n4(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 4);
  }
}


void n5(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 5);
  }
}


void n6(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 6);
  }
}


void n7(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 7);
  }
}


void n8(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 8);
  }
}


void n9(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 9);
  }
}


void n10(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 10);
  }
}


void n11(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 11);
  }
}


void n12(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 12);
  }
}


void n13(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 13);
  }
}


void n14(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 14);
  }
}


void n15(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 15);
  }
}


void n16(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 16);
  }
}


void n17(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 17);
  }
}


void n18(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 18);
  }
}


void n19(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 19);
  }
}


void n20(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 20);
  }
}


void n21(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 21);
  }
}


void n22(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 22);
  }
}


void n23(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 23);
  }
}


void n24(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 24);
  }
}


void n25(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 25);
  }
}


void n26(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 26);
  }
}


void n27(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 27);
  }
}


void n28(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 28);
  }
}


void n29(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 29);
  }
}


void n30(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 30);
  }
}


void n31(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 31);
  }
}


void n32(output_stream<int32> * out1) {
  for (int32 i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    writeincr(out1, 32);
  }
}


void n33(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 1
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 9
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n34(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 2
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 10
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n35(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 3
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 11
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n36(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 4
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 12
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n37(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 5
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 13
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n38(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 6
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 14
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n39(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 7
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 15
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n40(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 8
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 16
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n41(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 17
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 25
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n42(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 18
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 26
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n43(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 19
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 27
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n44(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 20
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 28
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n45(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 21
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 29
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n46(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 22
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 30
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n47(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 23
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 31
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n48(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 24
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 32
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n49(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 1
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 9
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 17
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 25
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n50(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 2
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 10
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 18
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 26
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n51(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 3
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 11
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 19
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 27
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n52(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 4
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 12
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 20
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 28
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n53(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 5
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 13
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 21
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 29
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n54(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 6
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 14
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 22
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 30
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n55(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 7
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 15
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 23
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 31
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n56(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 8
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 16
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 24
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 32
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n57(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 1
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 2
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 9
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 10
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 17
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 18
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 25
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 26
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n58(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 3
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 4
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 11
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 12
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 19
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 20
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 27
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 28
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n59(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 1
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 2
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 3
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 4
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 9
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 10
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 11
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 12
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 17
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 18
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 19
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 20
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 25
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 26
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 27
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 28
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n60(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 5
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 6
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 7
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 8
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 13
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 14
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 15
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 16
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 21
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 22
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 23
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 24
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 29
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 30
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 31
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 32
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n61(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 5
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 6
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 13
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 14
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 21
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 22
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 29
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 30
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n62(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<int32> * restrict out1) {
  // 7
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 8
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 15
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 16
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 23
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 24
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 31
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
    WRITEINCR(MS_rsrc1, out1, data);
  }

  // 32
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
    WRITEINCR(MS_rsrc1, out1, data);
  }
}


void n63(input_stream<int32> * restrict in1, input_stream<int32> * restrict in2, output_stream<uint64> * out) {
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;
  time1 = tile.cycles();

  // 1
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }
  
  // 2
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 3
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 4
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 5
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 6
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 7
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 8
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 9
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 10
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 11
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 12
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 13
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 14
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 15
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 16
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 17
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 18
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 19
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 20
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 21
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 22
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 23
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 24
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 25
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 26
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 27
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 28
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in1);
  }

  // 29
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 30
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 31
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  // 32
  for (int i = 0; i < N; i++)
  chess_prepare_for_pipelining {
    int32_t data = READINCR(SS_rsrc1, in2);
  }

  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out, time);
}