#include <adf.h>
#include "kernels.h"

using namespace adf;

class simpleGraph : public graph {
private:
  kernel kernel_list[63];

public:
  output_gmio gmio_time;

  simpleGraph() {
    gmio_time = output_gmio::create("gmio_time", 64, 1000);

    kernel_list[0] = kernel::create(n1);
    location<kernel>(kernel_list[0]) = tile(0, 0);

    kernel_list[1] = kernel::create(n2);
    location<kernel>(kernel_list[1]) = tile(0, 1);

    kernel_list[2] = kernel::create(n3);
    location<kernel>(kernel_list[2]) = tile(0, 2);

    kernel_list[3] = kernel::create(n4);
    location<kernel>(kernel_list[3]) = tile(0, 3);

    kernel_list[4] = kernel::create(n5);
    location<kernel>(kernel_list[4]) = tile(0, 4);

    kernel_list[5] = kernel::create(n6);
    location<kernel>(kernel_list[5]) = tile(0, 5);

    kernel_list[6] = kernel::create(n7);
    location<kernel>(kernel_list[6]) = tile(0, 6);

    kernel_list[7] = kernel::create(n8);
    location<kernel>(kernel_list[7]) = tile(0, 7);

    kernel_list[8] = kernel::create(n9);
    location<kernel>(kernel_list[8]) = tile(1, 0);

    kernel_list[9] = kernel::create(n10);
    location<kernel>(kernel_list[9]) = tile(1, 1);

    kernel_list[10] = kernel::create(n11);
    location<kernel>(kernel_list[10]) = tile(1, 2);

    kernel_list[11] = kernel::create(n12);
    location<kernel>(kernel_list[11]) = tile(1, 3);

    kernel_list[12] = kernel::create(n13);
    location<kernel>(kernel_list[12]) = tile(1, 4);

    kernel_list[13] = kernel::create(n14);
    location<kernel>(kernel_list[13]) = tile(1, 5);

    kernel_list[14] = kernel::create(n15);
    location<kernel>(kernel_list[14]) = tile(1, 6);

    kernel_list[15] = kernel::create(n16);
    location<kernel>(kernel_list[15]) = tile(1, 7);

    kernel_list[16] = kernel::create(n17);
    location<kernel>(kernel_list[16]) = tile(3, 0);

    kernel_list[17] = kernel::create(n18);
    location<kernel>(kernel_list[17]) = tile(3, 1);

    kernel_list[18] = kernel::create(n19);
    location<kernel>(kernel_list[18]) = tile(3, 2);

    kernel_list[19] = kernel::create(n20);
    location<kernel>(kernel_list[19]) = tile(3, 3);

    kernel_list[20] = kernel::create(n21);
    location<kernel>(kernel_list[20]) = tile(3, 4);

    kernel_list[21] = kernel::create(n22);
    location<kernel>(kernel_list[21]) = tile(3, 5);

    kernel_list[22] = kernel::create(n23);
    location<kernel>(kernel_list[22]) = tile(3, 6);

    kernel_list[23] = kernel::create(n24);
    location<kernel>(kernel_list[23]) = tile(3, 7);

    kernel_list[24] = kernel::create(n25);
    location<kernel>(kernel_list[24]) = tile(4, 0);

    kernel_list[25] = kernel::create(n26);
    location<kernel>(kernel_list[25]) = tile(4, 1);

    kernel_list[26] = kernel::create(n27);
    location<kernel>(kernel_list[26]) = tile(4, 2);

    kernel_list[27] = kernel::create(n28);
    location<kernel>(kernel_list[27]) = tile(4, 3);

    kernel_list[28] = kernel::create(n29);
    location<kernel>(kernel_list[28]) = tile(4, 4);

    kernel_list[29] = kernel::create(n30);
    location<kernel>(kernel_list[29]) = tile(4, 5);

    kernel_list[30] = kernel::create(n31);
    location<kernel>(kernel_list[30]) = tile(4, 6);

    kernel_list[31] = kernel::create(n32);
    location<kernel>(kernel_list[31]) = tile(4, 7);

    kernel_list[32] = kernel::create(n33);
    location<kernel>(kernel_list[32]) = tile(2, 0);

    kernel_list[33] = kernel::create(n34);
    location<kernel>(kernel_list[33]) = tile(2, 1);

    kernel_list[34] = kernel::create(n35);
    location<kernel>(kernel_list[34]) = tile(2, 2);

    kernel_list[35] = kernel::create(n36);
    location<kernel>(kernel_list[35]) = tile(2, 3);

    kernel_list[36] = kernel::create(n37);
    location<kernel>(kernel_list[36]) = tile(2, 4);

    kernel_list[37] = kernel::create(n38);
    location<kernel>(kernel_list[37]) = tile(2, 5);

    kernel_list[38] = kernel::create(n39);
    location<kernel>(kernel_list[38]) = tile(2, 6);

    kernel_list[39] = kernel::create(n40);
    location<kernel>(kernel_list[39]) = tile(2, 7);

    kernel_list[40] = kernel::create(n41);
    location<kernel>(kernel_list[40]) = tile(5, 0);

    kernel_list[41] = kernel::create(n42);
    location<kernel>(kernel_list[41]) = tile(5, 1);

    kernel_list[42] = kernel::create(n43);
    location<kernel>(kernel_list[42]) = tile(5, 2);

    kernel_list[43] = kernel::create(n44);
    location<kernel>(kernel_list[43]) = tile(5, 3);

    kernel_list[44] = kernel::create(n45);
    location<kernel>(kernel_list[44]) = tile(5, 4);

    kernel_list[45] = kernel::create(n46);
    location<kernel>(kernel_list[45]) = tile(5, 5);

    kernel_list[46] = kernel::create(n47);
    location<kernel>(kernel_list[46]) = tile(5, 6);

    kernel_list[47] = kernel::create(n48);
    location<kernel>(kernel_list[47]) = tile(5, 7);

    kernel_list[48] = kernel::create(n49);
    location<kernel>(kernel_list[48]) = tile(6, 0);

    kernel_list[49] = kernel::create(n50);
    location<kernel>(kernel_list[49]) = tile(6, 1);

    kernel_list[50] = kernel::create(n51);
    location<kernel>(kernel_list[50]) = tile(6, 2);

    kernel_list[51] = kernel::create(n52);
    location<kernel>(kernel_list[51]) = tile(6, 3);

    kernel_list[52] = kernel::create(n53);
    location<kernel>(kernel_list[52]) = tile(6, 4);

    kernel_list[53] = kernel::create(n54);
    location<kernel>(kernel_list[53]) = tile(6, 5);

    kernel_list[54] = kernel::create(n55);
    location<kernel>(kernel_list[54]) = tile(6, 6);

    kernel_list[55] = kernel::create(n56);
    location<kernel>(kernel_list[55]) = tile(6, 7);

    kernel_list[56] = kernel::create(n57);
    location<kernel>(kernel_list[56]) = tile(7, 1);

    kernel_list[57] = kernel::create(n58);
    location<kernel>(kernel_list[57]) = tile(7, 2);

    kernel_list[58] = kernel::create(n59);
    location<kernel>(kernel_list[58]) = tile(7, 3);

    kernel_list[59] = kernel::create(n60);
    location<kernel>(kernel_list[59]) = tile(7, 4);

    kernel_list[60] = kernel::create(n61);
    location<kernel>(kernel_list[60]) = tile(7, 5);

    kernel_list[61] = kernel::create(n62);
    location<kernel>(kernel_list[61]) = tile(7, 6);

    kernel_list[62] = kernel::create(n63);
    location<kernel>(kernel_list[62]) = tile(8, 3);

    
    for (int i = 0; i < 63; i++) {
      source(kernel_list[i]) = "kernels.cpp";
      runtime<ratio>(kernel_list[i]) = 1;
    }



    connect<stream> (kernel_list[0].out[0], kernel_list[32].in[0]);
    connect<stream> (kernel_list[1].out[0], kernel_list[33].in[0]);
    connect<stream> (kernel_list[2].out[0], kernel_list[34].in[0]);
    connect<stream> (kernel_list[3].out[0], kernel_list[35].in[0]);
    connect<stream> (kernel_list[4].out[0], kernel_list[36].in[0]);
    connect<stream> (kernel_list[5].out[0], kernel_list[37].in[0]);
    connect<stream> (kernel_list[6].out[0], kernel_list[38].in[0]);
    connect<stream> (kernel_list[7].out[0], kernel_list[39].in[0]);
    connect<stream> (kernel_list[8].out[0], kernel_list[32].in[1]);
    connect<stream> (kernel_list[9].out[0], kernel_list[33].in[1]);
    connect<stream> (kernel_list[10].out[0], kernel_list[34].in[1]);
    connect<stream> (kernel_list[11].out[0], kernel_list[35].in[1]);
    connect<stream> (kernel_list[12].out[0], kernel_list[36].in[1]);
    connect<stream> (kernel_list[13].out[0], kernel_list[37].in[1]);
    connect<stream> (kernel_list[14].out[0], kernel_list[38].in[1]);
    connect<stream> (kernel_list[15].out[0], kernel_list[39].in[1]);
    connect<stream> (kernel_list[16].out[0], kernel_list[40].in[0]);
    connect<stream> (kernel_list[17].out[0], kernel_list[41].in[0]);
    connect<stream> (kernel_list[18].out[0], kernel_list[42].in[0]);
    connect<stream> (kernel_list[19].out[0], kernel_list[43].in[0]);
    connect<stream> (kernel_list[20].out[0], kernel_list[44].in[0]);
    connect<stream> (kernel_list[21].out[0], kernel_list[45].in[0]);
    connect<stream> (kernel_list[22].out[0], kernel_list[46].in[0]);
    connect<stream> (kernel_list[23].out[0], kernel_list[47].in[0]);
    connect<stream> (kernel_list[24].out[0], kernel_list[40].in[1]);
    connect<stream> (kernel_list[25].out[0], kernel_list[41].in[1]);
    connect<stream> (kernel_list[26].out[0], kernel_list[42].in[1]);
    connect<stream> (kernel_list[27].out[0], kernel_list[43].in[1]);
    connect<stream> (kernel_list[28].out[0], kernel_list[44].in[1]);
    connect<stream> (kernel_list[29].out[0], kernel_list[45].in[1]);
    connect<stream> (kernel_list[30].out[0], kernel_list[46].in[1]);
    connect<stream> (kernel_list[31].out[0], kernel_list[47].in[1]);
    connect<stream> (kernel_list[32].out[0], kernel_list[48].in[0]);
    connect<stream> (kernel_list[33].out[0], kernel_list[49].in[0]);
    connect<stream> (kernel_list[34].out[0], kernel_list[50].in[0]);
    connect<stream> (kernel_list[35].out[0], kernel_list[51].in[0]);
    connect<stream> (kernel_list[36].out[0], kernel_list[52].in[0]);
    connect<stream> (kernel_list[37].out[0], kernel_list[53].in[0]);
    connect<stream> (kernel_list[38].out[0], kernel_list[54].in[0]);
    connect<stream> (kernel_list[39].out[0], kernel_list[55].in[0]);
    connect<stream> (kernel_list[40].out[0], kernel_list[48].in[1]);
    connect<stream> (kernel_list[41].out[0], kernel_list[49].in[1]);
    connect<stream> (kernel_list[42].out[0], kernel_list[50].in[1]);
    connect<stream> (kernel_list[43].out[0], kernel_list[51].in[1]);
    connect<stream> (kernel_list[44].out[0], kernel_list[52].in[1]);
    connect<stream> (kernel_list[45].out[0], kernel_list[53].in[1]);
    connect<stream> (kernel_list[46].out[0], kernel_list[54].in[1]);
    connect<stream> (kernel_list[47].out[0], kernel_list[55].in[1]);
    connect<stream> (kernel_list[48].out[0], kernel_list[56].in[0]);
    connect<stream> (kernel_list[49].out[0], kernel_list[56].in[1]);
    connect<stream> (kernel_list[50].out[0], kernel_list[57].in[0]);
    connect<stream> (kernel_list[51].out[0], kernel_list[57].in[1]);
    connect<stream> (kernel_list[52].out[0], kernel_list[60].in[0]);
    connect<stream> (kernel_list[53].out[0], kernel_list[60].in[1]);
    connect<stream> (kernel_list[54].out[0], kernel_list[61].in[0]);
    connect<stream> (kernel_list[55].out[0], kernel_list[61].in[1]);
    connect<stream> (kernel_list[56].out[0], kernel_list[58].in[0]);
    connect<stream> (kernel_list[57].out[0], kernel_list[58].in[1]);
    connect<stream> (kernel_list[58].out[0], kernel_list[62].in[0]);
    connect<stream> (kernel_list[59].out[0], kernel_list[62].in[1]);
    connect<stream> (kernel_list[60].out[0], kernel_list[59].in[0]);
    connect<stream> (kernel_list[61].out[0], kernel_list[59].in[1]);
    connect<stream> (kernel_list[62].out[0], gmio_time.in[0]);
    
  }
};

