#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void sender(input_pktstream *in, output_pktstream *out);
void receiver(input_pktstream *in, output_pktstream *out);
#endif
