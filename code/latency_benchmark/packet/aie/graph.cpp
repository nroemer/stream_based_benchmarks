
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <stdio.h>


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

#define __TIMER__
#endif

#ifdef __TIMER__
#include <chrono>
#include <ctime>
class Timer
{
  std::chrono::high_resolution_clock::time_point mTimeStart;

public:
  Timer() { reset(); }
  long long stop()
  {
    std::chrono::high_resolution_clock::time_point timeEnd =
        std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                 mTimeStart)
        .count();
  }
  void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};

#endif

simpleGraph mygraph;
int main(int argc, char *argv[])
{
  adf::return_code ret;

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif


  mygraph.init();


#ifdef __TIMER__
  // timing
  Timer timer;
#endif
  ret = mygraph.run(1);
  if (ret != adf::ok)
  {
    printf("Host: Run failed\n");
    return ret;
  }

  mygraph.wait(); // wait for iteration(s) to finish
#ifdef __TIMER__
  long long timer_stop = timer.stop();
  printf("Execution time (usecs): %lld\n", timer_stop);
#endif

  mygraph.run(0); // added
  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
