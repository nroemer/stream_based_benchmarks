#include <adf.h>
#include "kernels.h"


// packetstream setup
const uint32 pktType0 = 0;
const uint32 pktType1 = 1;


void sender(input_pktstream *in, output_pktstream *out)
{
  int32 data = 7;  

  // packetstream setup write
  uint32 ID = getPacketid(out, 0);
  writeHeader(out, pktType0, ID); 

  writeincr(out, data, true);
  

  // packetstream setup read
  readincr(in);
  bool tlast;
  data = readincr(in, tlast);

  asm volatile ("" ::: "memory");
}


void receiver(input_pktstream *in, output_pktstream *out)
{
  int32 data;

  // packetstream setup read
  readincr(in);
  bool tlast;
  data = readincr(in, tlast);
   

  // packetstream setup write
  uint32 ID = getPacketid(out, 0);
  writeHeader(out, pktType1, ID); 

  writeincr(out, data, true);
  
  asm volatile ("" ::: "memory");
}
