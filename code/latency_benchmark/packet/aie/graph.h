
#include <adf.h>
#include "kernels.h"

using namespace adf;


class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel receiverk;

public:
  simpleGraph()
  {
    senderk = kernel::create(sender);
    receiverk = kernel::create(receiver);

    connect<pktstream> (senderk.out[0], receiverk.in[0]);
    connect<pktstream> (receiverk.out[0], senderk.in[0]);

    source(senderk) = "kernels.cpp";
    source(receiverk) = "kernels.cpp";

     location<kernel>(senderk) = tile(25, 1);
     location<kernel>(receiverk) = tile(26, 1);

    
    // location<kernel>(senderk) = tile(25, 1);
    // location<kernel>(receiverk) = tile(26, 2);


    // location<kernel>(senderk) = tile(25, 0);
    // location<kernel>(receiverk) = tile(29, 0);

  
    runtime<ratio>(senderk) = 1;
    runtime<ratio>(receiverk) = 1;
  }
};
