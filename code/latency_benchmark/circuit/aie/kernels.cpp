#include <adf.h>
#include "kernels.h"


#define N 100000

void first(input_stream<int32> *in, output_stream<int32> *out, output_stream<uint64> *out2)
{
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 data = 0;

  time1 = tile.cycles();
  for (unsigned i = 0; i < N; i++) {
    writeincr(out, data);
    data = readincr(in);
    asm volatile ("" ::: "memory");
  }
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out2, time);
}


void second(input_stream<int32> *in, output_stream<int32> *out)
{

  int32 data;

  for (unsigned i = 0; i < N; i++) {
    data = readincr(in);
    writeincr(out,data);
    asm volatile ("" ::: "memory");
  }
}
