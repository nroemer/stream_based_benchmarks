
#include <adf.h>
#include "kernels.h"

using namespace adf;


class simpleGraph : public graph
{
private:
  kernel firstk;
  kernel secondk;

public:
  output_gmio gmio_time;
  simpleGraph()
  {

  
    firstk = kernel::create(first);
    secondk = kernel::create(second);

    
    connect<stream> net0(firstk.out[0], secondk.in[0]);
    connect<stream> net1(secondk.out[0], firstk.in[0]);

    source(firstk) = "kernels.cpp";
    source(secondk) = "kernels.cpp";
     

    location<kernel>(firstk) = tile(0, 0);
    location<kernel>(secondk) = tile(1, 0);

    
    runtime<ratio>(firstk) = 1;
    runtime<ratio>(secondk) = 1;

    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> (firstk.out[1], gmio_time.in[0]);
  }
};
