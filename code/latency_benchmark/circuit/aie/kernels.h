#ifndef FUNCTION_KERNELS_H
#define FUNCTION_KERNELS_H
#include "aie_api/aie.hpp"
#include "aie_api/aie_adf.hpp"
void first(input_stream<int32> *in, output_stream<int32> *out, output_stream<uint64> *out2);
void second(input_stream<int32> *in, output_stream<int32> *out);
#endif
