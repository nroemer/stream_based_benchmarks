#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <adf.h>

using namespace adf;
simpleGraph mygraph;


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"
#endif



int main(int argc, char *argv[])
{
  // Setup standard values
  adf::return_code ret;

  // Setup benchmark data file
  std::string csvName = "P2P-stream-based-latency.csv";
  std::ofstream ResultFile(csvName);
  ResultFile << "latency [nsec]\n";

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2) {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  uint64_t out_size = sizeof(uint64_t);
  uint64 *tmp = (uint64 *)GMIO::malloc(out_size);

  mygraph.init();

  mygraph.gmio_time.aie2gm_nb(tmp, out_size);

  ret = mygraph.run(1);
  
  if (ret != adf::ok) {
    printf("Host: Run failed\n");
    return ret;
  }
   
  mygraph.gmio_time.wait();
  mygraph.wait(); // wait for iteration(s) to finish
  
  printf("%llu\n", tmp[0]/200000);
   
  ResultFile << (tmp[0]/200000) << std::endl;
   
  ResultFile.close();

  GMIO::free(tmp);

  ret = mygraph.end();
  if (ret != adf::ok) {
      printf("Host: End failed\n");
      return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
