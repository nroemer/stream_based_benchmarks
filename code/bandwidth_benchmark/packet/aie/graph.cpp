
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <stdio.h>


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

#define __TIMER__
#endif

#ifdef __TIMER__
#include <chrono>
#include <ctime>
class Timer
{
  std::chrono::high_resolution_clock::time_point mTimeStart;

public:
  Timer() { reset(); }
  long long stop()
  {
    std::chrono::high_resolution_clock::time_point timeEnd =
        std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(timeEnd -
                                                                 mTimeStart)
        .count();
  }
  void reset() { mTimeStart = std::chrono::high_resolution_clock::now(); }
};

#endif

const uint64 sizes[21] = {
    1,
    10,
    20,
    40,
    80,
    125,
    250,
    500,
    1000,
    2000,       //   8 KB
    4000,       //  16 KB
    8000,       //  32 KB
    16000,       //  64 KB
    32000,      // 128 KB
    64000,      // 256 KB
    128000,      // 512 KB
    256000,     //   1 MB
    512000,     //   2 MB
    1024000,     //   4 MB
    2048000,    //   8 MB
    4096000    //  16 MB

};

simpleGraph mygraph;
int main(int argc, char *argv[])
{
  adf::return_code ret;

  // Setup benchmark data file
  FILE * pFile;
  pFile = fopen("P2P-stream-based-non-neighbour-circuit.csv", "w");
  fprintf(pFile, "Data,worst,best,median\n");

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  const int redos = 1000;
  double throughput[redos];
  uint64 message_size;
  double worst, best, mean;
  
  for (int i = 0; i < (sizeof(sizes)/sizeof(long long)); i++) {
    message_size = sizes[i];

    std::cout << "Size" << message_size << std::endl;
    for (int j = 0; j < redos; j++)
    {
      
      mygraph.init();

      mygraph.update(mygraph.size_value_rcv, message_size);
      mygraph.update(mygraph.size_value_snd, message_size);

#ifdef __TIMER__
      // timing
      Timer timer;
#endif
      ret = mygraph.run(1);
      if (ret != adf::ok)
      {
        printf("Host: Run failed\n");
        return ret;
      }

      mygraph.wait(); // wait for iteration(s) to finish
#ifdef __TIMER__
      long long timer_stop = timer.stop();
      throughput[j] = ((message_size * 4) / timer_stop);
#endif
      }

  
  worst = throughput[0];
  best = throughput[0];
  median = 0.0;
  uint64_t array [1000];
  
  for (int i = 0; i < redos; i++) {
    array[i] = throughput[i];
    worst = throughput[i] > worst ? throughput[i] : worst;
    best = throughput[i] > best ? best : throughput[i];
  }
  
  int n = sizeof(array) / sizeof(array[0]);
  std::sort(array, array + n);
  if (n % 2 != 0) {
    median = array[n/2];
  }
  else {
    median = (array[(n-1)/2] + array[n/2])/2.0;
  }
  
  fprintf(pFile, "%llu,%f,%f,%f\n", (message_size * 4), worst, best, median);

  }

  fclose(pFile);

  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
