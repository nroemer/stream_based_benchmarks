/* A simple kernel
 */
#include <adf.h>
#include "kernels.h"


// packetstream setup
const uint32 pktType = 0;


/**
  Sender kernel: sends "1"s to all of the receivers
  The amount of sent "1"s is defined via the "size" function argument
*/

void sender(output_pktstream *out, uint64 size)
{
  const uint64_t length = size;
  
  // packetstream setup write
  uint32 ID = getPacketid(out, 0);
  writeHeader(out, pktType, ID); 


  const int32 data = 1;
  for (unsigned i = 0; i < length; i++)
    writeincr(out, data);
  
}

/**
  Receiving kernel: receives all the data sent from the sending kernel
  The exact amount is specified via the "size" function argument. 
*/
void receiver(input_pktstream *in, uint64 size)
{
  int32 receiving_data;

  const uint64_t length = size;

  // packetstream setup read
  readincr(in);


  for (unsigned i = 0; i < length; i++)
    receiving_data = readincr(in);
   
}
