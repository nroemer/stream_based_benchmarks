
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "graph.h"
#include <stdio.h>


#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
#include "adf/adf_api/XRTConfig.h"
#include "experimental/xrt_kernel.h"

#endif

const uint64 sizes[32] = {
    1,
    10,
    20,
    40,
    80,
    125,
    250,
    500,
    1000,
    2000,       //   8 KB
    4000,       //  16 KB
    8000,       //  32 KB
    16000,       //  64 KB
    32000,      // 128 KB
    64000,      // 256 KB
    128000,      // 512 KB
    256000,     //   1 MB
    512000,     //   2 MB
    1024000,     //   4 MB
    2048000,    //   8 MB
    4096000,    //  16 MB
    8192000,    //  32 MB
    16384000,    //  64 MB
    32768000,   // 128 MB
    65536000,   // 256 MB
    131072000,   // 512 MB
    262144000,  //   1 GB
    524288000,  //   2 GB
    1048576000,  //   4 GB
    2097152000, //   8 GB  
    4194304000, //  16 GB
    8388608000  //  32 GB
};

simpleGraph mygraph;
int main(int argc, char *argv[])
{
  adf::return_code ret;

  // Setup benchmark data file
  FILE * pFile;
  pFile = fopen("P2P-stream-based-non-neighbour-circuit.csv", "w");
  fprintf(pFile, "Data,worst,best,mean\n");

// Get user input only if not in x86simulation or aiesimulation
#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  if (argc != 2)
  {
    std::cerr << "P2P" << std::endl;
    std::cerr << "Usage: " << argv[0] << " <xclbin file>" << std::endl;
    exit(-1);
  }
  std::string xclbinFilename = argv[1];
  
  // Create XRT device handle for ADF API
  auto dhdl = xrtDeviceOpen(0);
  std::cout << "Loading binary file " << xclbinFilename << std::endl;
  xrtDeviceLoadXclbinFile(dhdl, xclbinFilename.c_str());
  xuid_t uuid;
  xrtDeviceGetXclbinUUID(dhdl, uuid);
  adf::registerXRT(dhdl, uuid);
#endif

  const int redos = 1000;
  uint64 message_size;
  uint64 worst, best, mean;

  uint64_t out_size = sizeof(uint64_t);
  
  uint64 *time = (uint64 *)GMIO::malloc(4 * out_size);
  uint64 *tmp = (uint64 *)GMIO::malloc(out_size);


  
  for (int i = 0; i < (sizeof(sizes)/sizeof(long long)); i++) {
    message_size = sizes[i];

      
    mygraph.init();
    mygraph.gmio_time.aie2gm_nb(tmp, out_size);
    mygraph.update(mygraph.size_value_rcv, message_size);
    mygraph.update(mygraph.size_value_snd, message_size);

    ret = mygraph.run(1);
    if (ret != adf::ok)
    {
      printf("Host: Run failed\n");
      return ret;
    }
    mygraph.gmio_time.wait();
    mygraph.wait(); // wait for iteration(s) to finish

    time[i] = tmp[0];

  
    fprintf(pFile, "%llu,%llu,%llu\n", (message_size * 4), tmp[0], time[i]);
    printf("%llu\n", tmp[0]);
    printf("%llu\n", time[i]);
  }

  fclose(pFile);

  GMIO::free(time);
  GMIO::free(tmp);

  ret = mygraph.end();
  if (ret != adf::ok)
  {
    printf("End failed\n");
    return ret;
  }

#if !defined(__AIESIM__) && !defined(__X86SIM__) && !defined(__ADF_FRONTEND__)
  xrtDeviceClose(dhdl);
#endif
  return 0;
}
