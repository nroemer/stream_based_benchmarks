/* A simple kernel
 */
#include <adf.h>
#include "kernels.h"

/**
  Sender kernel: sends "1"s to all of the receivers
  The amount of sent "1"s is defined via the "size" function argument
*/

void sender(output_stream<int32> *out, uint64 size)
{
  const uint64_t length = size;

  const int32 data = 1;
  for (unsigned i = 0; i < length; i++)
    writeincr(out, data);
  
}

/**
  Receiving kernel: receives all the data sent from the sending kernel
  The exact amount is specified via the "size" function argument. 
*/
void receiver(input_stream<int32> *in, output_stream<uint64> *out, uint64 size)
{
  aie::tile tile = aie::tile::current();
  uint64 time1;
  uint64 time2;

  int32 receiving_data;

  time1 = tile.cycles();
  const uint64_t length = size;

  for (unsigned i = 0; i < length; i++)
    receiving_data = readincr(in);
  
  time2 = tile.cycles();
  uint64 time = time2 - time1;
  writeincr(out, time);

}
