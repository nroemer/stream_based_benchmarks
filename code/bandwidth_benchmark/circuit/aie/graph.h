
#include <adf.h>
#include "kernels.h"

using namespace adf;

// This is a basic graph creating two kernels, one sender and one receiver, and then connecting them
class simpleGraph : public graph
{
private:
  kernel senderk;
  kernel receiverk;

public:
  input_port size_value_snd, size_value_rcv;
  output_gmio gmio_time;
  simpleGraph()
  {
    // Specify which kernel is which kernel function from kernels.cpp
    senderk = kernel::create(sender);
    receiverk = kernel::create(receiver);

    // data stream from sender to receiver
    connect<stream> net0(senderk.out[0], receiverk.in[0]);

    // runtime parameters
    connect<parameter>(size_value_snd, async(senderk.in[0]));
    connect<parameter>(size_value_rcv, async(receiverk.in[1]));

    source(senderk) = "kernels.cpp";
    source(receiverk) = "kernels.cpp";

    // Put some location constraints on the kernels; choose which scenario you wanna use:
    // Sender and receiver are direct neighbours
    location<kernel>(senderk) = tile(0, 0);
    location<kernel>(receiverk) = tile(1, 0);

    // Sender and receiver are non-neighbouring
    //location<kernel>(senderk) = tile(0, 0);
    //location<kernel>(receiverk) = tile(49, 7);

    // make sure that they don't share the same AI-Engine
    runtime<ratio>(senderk) = 1;
    runtime<ratio>(receiverk) = 1;

    gmio_time = output_gmio::create("gmio_time", 64, 1000);
    connect<stream> (receiverk.out[0], gmio_time.in[0]);

  }
};
